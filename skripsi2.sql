-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 10, 2022 at 12:19 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skripsi2`
--

-- --------------------------------------------------------

--
-- Table structure for table `jointender`
--

CREATE TABLE `jointender` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `tenderId` int(11) NOT NULL,
  `jabatan` varchar(255) DEFAULT NULL,
  `statusSeleksi` enum('LOLOS','TIDAK LOLOS') DEFAULT NULL,
  `statusDokumen` enum('LOLOS','TIDAK LOLOS') DEFAULT NULL,
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jointender`
--

INSERT INTO `jointender` (`id`, `userId`, `tenderId`, `jabatan`, `statusSeleksi`, `statusDokumen`, `createdAt`) VALUES
(163, 8, 33, 'QE', NULL, NULL, '2022-06-24 00:01:13'),
(164, 9, 33, 'QE', NULL, NULL, '2022-06-24 00:01:32'),
(165, 10, 33, 'QE', NULL, NULL, '2022-06-24 00:05:22'),
(166, 11, 33, 'QE', NULL, NULL, '2022-06-24 00:05:45'),
(167, 12, 33, 'QE', NULL, NULL, '2022-06-24 00:06:09'),
(168, 13, 33, 'SE', NULL, NULL, '2022-06-24 00:06:29'),
(169, 15, 33, 'SE', NULL, NULL, '2022-06-24 00:07:20'),
(170, 17, 33, 'SE', NULL, NULL, '2022-06-24 00:07:52'),
(171, 16, 33, 'SE', NULL, NULL, '2022-06-24 00:09:18'),
(172, 14, 33, 'SE', NULL, NULL, '2022-06-24 00:10:48');

-- --------------------------------------------------------

--
-- Table structure for table `pajak`
--

CREATE TABLE `pajak` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `npwp` varchar(255) DEFAULT NULL,
  `scanNpwp` varchar(255) DEFAULT NULL,
  `fileSptTahunan` varchar(255) DEFAULT NULL,
  `fileSptBulanan` varchar(255) DEFAULT NULL,
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `updatedAt` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pajak`
--

INSERT INTO `pajak` (`id`, `userId`, `npwp`, `scanNpwp`, `fileSptTahunan`, `fileSptBulanan`, `createdAt`, `updatedAt`) VALUES
(1, 3, NULL, NULL, NULL, NULL, '2021-09-16 13:47:51', NULL),
(3, 5, NULL, NULL, NULL, NULL, '2021-10-23 08:23:30', NULL),
(4, 6, NULL, NULL, NULL, NULL, '2021-12-05 09:54:46', NULL),
(5, 7, '89687', '2021-12-05T09-57-54.858Zdemikian.jpg', '2021-12-05T09-57-54.918Zdemikian.jpg', '2021-12-05T09-57-54.881Zhal 9.jpg', '2021-12-05 09:55:05', '2021-12-05 09:57:54'),
(6, 8, '082739210801000', '2021-12-05T19-32-46.572Znpwp nasri.png', '2021-12-05T19-32-46.653Zpajak nasri.png', '2021-12-26T12-23-05.959Zpajak.png', '2021-12-05 19:16:47', '2022-01-10 21:34:05'),
(7, 9, '47.300.271.5-802.000', '2021-12-05T20-29-52.938Znpwp.png', '2021-12-26T12-28-05.376Zpajak.png', '2021-12-26T12-28-05.373Zpajak.png', '2021-12-05 20:24:23', '2021-12-26 12:28:05'),
(8, 10, '07.745-136.7-801.000', '2021-12-06T10-51-07.481Znpwp.png', '2021-12-06T10-51-07.534Zspt paja.png', '2021-12-06T10-51-07.501Zspt tahunan.png', '2021-12-06 06:14:00', '2021-12-26 12:29:57'),
(9, 11, '07.745-136.7-801.000', '2021-12-06T06-19-22.898Znpwp.png', '2021-12-06T06-19-23.027Zspt tahunan.png', '2021-12-06T06-19-22.977Zspt paja.png', '2021-12-06 06:14:29', '2021-12-06 06:19:23'),
(10, 12, '641318092822000', '2021-12-06T10-07-03.865Znpwp.png', '2021-12-06T10-07-03.949Zspt tahunan.png', '2021-12-26T12-42-57.444Zpajak.png', '2021-12-06 07:08:10', '2021-12-26 12:42:57'),
(11, 13, '575040779402000', '2021-12-06T11-13-46.547Znpwp.png', '2021-12-07T17-51-21.234Znpwp.png', '2021-12-07T17-51-21.217Znpwp.png', '2021-12-06 11:09:18', '2021-12-07 17:51:21'),
(12, 14, '66.736.213.1-822.000', '2021-12-07T17-59-55.459Znpwp.png', '2021-12-07T17-59-55.532Zpajak.png', '2021-12-07T17-59-55.460Zpajak.png', '2021-12-07 17:53:08', '2021-12-07 17:59:55'),
(13, 15, '08.277.122.1-801.000', '2021-12-07T18-39-20.296Znpwp.png', '2021-12-07T18-39-20.334Znpwp.png', '2021-12-07T18-39-20.300Znpwp.png', '2021-12-07 18:16:28', '2021-12-07 18:39:20'),
(14, 16, '14.538.568.8-809.000', '2021-12-19T21-34-18.269Zspt dan npwp.png', '2021-12-19T21-34-18.309Zspt dan npwp.png', '2021-12-19T21-34-18.303Zspt dan npwp.png', '2021-12-19 21:30:24', '2021-12-19 21:34:18'),
(15, 17, '543433965814000', '2021-12-20T08-20-00.351Zpajak dan npwp.png', '2021-12-20T08-20-00.357Zpajak dan npwp.png', '2021-12-20T08-20-00.353Zpajak dan npwp.png', '2021-12-19 21:51:41', '2021-12-20 08:20:00');

-- --------------------------------------------------------

--
-- Table structure for table `pengalaman`
--

CREATE TABLE `pengalaman` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL,
  `tanggalMulai` date DEFAULT NULL,
  `tanggalSelesai` date DEFAULT NULL,
  `fileBuktiKerja` varchar(255) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `updatedAt` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengalaman`
--

INSERT INTO `pengalaman` (`id`, `userId`, `nama`, `deskripsi`, `tanggalMulai`, `tanggalSelesai`, `fileBuktiKerja`, `createdAt`, `updatedAt`) VALUES
(1, 7, 'gedung', 'dki', '2021-12-05', '2021-12-05', '', '2021-12-05 09:58:29', NULL),
(2, 8, 'Paket Pelebaran Jalan Menuju Standar Paguyaman-Tabulo (KM. 72+725 s/d KM. 120+700)(SBSN)(MYC) TA. 2018-2019', '', '2018-09-15', '2019-12-31', '', '2021-12-05 19:34:43', NULL),
(3, 8, 'Pengantian Jembatan Mustika CS Provinsi Gorontalo Satker P2JN Gorontalo', '', '2018-02-14', '2018-11-24', '', '2021-12-05 19:35:39', NULL),
(4, 8, 'Pengawasan Teknis Penggantian Jembatan Tersebar  (Jbt. Sidomukti, Jbt. Sidomukti II, dan Jbt. Ayuluhi)  Provinsi Gorontalo', '', '2017-01-26', '2017-10-22', '', '2021-12-05 19:36:38', NULL),
(5, 8, 'Pengawasan Teknis Pembangunan Jembatan GORR I', '', '2016-02-18', '2016-12-13', '', '2021-12-05 19:38:11', NULL),
(6, 8, 'Pengawasan Teknis Penggantian Jembatan Ruas Taludaa (BTS. Prov. Sulut)-Pel. Gorontalo', '', '2015-03-01', '2015-10-10', '', '2021-12-05 19:39:12', NULL),
(7, 8, 'Pengawasan Jembatan Cibubur-Cileungsi Prov. Jawat Barat', '', '2014-02-01', '2014-12-26', '', '2021-12-05 19:39:58', NULL),
(8, 8, 'Pengawasan Teknis Jalan dan Jembatan Wamena', '', '2013-03-01', '2013-12-31', '', '2021-12-05 19:40:34', NULL),
(9, 8, 'Pengawasan Jembatan Dalam Kota Unahaa (Jl. Diponegoro)', 'Prov. Sulawesi Tenggara', '2012-02-05', '2012-12-06', '', '2021-12-05 19:41:15', NULL),
(10, 8, 'Pengawasan Jembatan Arma-Aiwahan', 'Prov. Maluku Utara', '2011-02-15', '2011-12-14', '', '2021-12-05 19:42:15', NULL),
(11, 8, 'Pembangunan Jalan dan Jembatan (DAK)', 'Kab. Toraja Utara', '2010-04-01', '2010-12-30', '', '2021-12-05 19:43:13', NULL),
(12, 8, 'Pengawasan Reguler Teknis Jalan dan Jembatan Sorong II di Kab. Sorong', 'Prov. Papua Barat', '2009-03-01', '2009-10-30', '', '2021-12-05 19:44:07', NULL),
(13, 8, 'Pengawasan Reguler Teknis jalan dan Jembatan I di Kab. Sorong', 'Prov. Papua Barat', '2008-03-01', '2008-11-17', '', '2021-12-05 19:44:51', NULL),
(14, 8, 'Pengawasan Jembatan Calang – Meulaboh (Jembatan : 752.12 M’)', 'Kab. Aceh Barat', '2007-04-01', '2007-12-15', '', '2021-12-05 19:46:06', NULL),
(15, 8, 'Pengawasan Teknis Jembatan Turirejo-Palon', 'Kab. Blora', '2006-02-01', '2006-12-31', '', '2021-12-05 19:46:46', NULL),
(16, 8, 'Pengawasan Teknis Jembatan Paket AJ-01', 'Pangkalpinang-Toboali', '2005-02-22', '2005-12-25', '', '2021-12-05 19:47:52', NULL),
(17, 8, 'Pengawasan Teknis Jembatan Surabaya-Waru-Juanda', 'Prov. Surabaya', '2004-02-12', '2004-12-14', '', '2021-12-05 19:48:43', NULL),
(18, 9, 'Core Team Perencanaan dan Pengawasan Provinsi Sulawesi Tengah', 'Provinsi Sulawesi Tengah', '2018-02-14', '2018-09-28', '', '2021-12-05 20:31:04', NULL),
(19, 9, 'Penggantian Jembatan Parigi II Cs', 'Provinsi Sulawesi Tengah', '2017-03-14', '2017-12-21', '', '2021-12-05 20:31:38', NULL),
(20, 9, 'Preservasi Rekonstruksi Jalan Tabone - Polewali', 'Provinsi Sulawesi Barat', '2016-01-10', '2016-10-21', '', '2021-12-05 20:32:17', NULL),
(21, 9, 'Pelebaran Jalan Ambesea – Lainea ', 'Provinsi Sulawesi Tenggara', '2015-04-15', '2015-11-30', '', '2021-12-05 20:33:07', NULL),
(22, 9, 'Pengawasan Teknik DAK+DAU Sub Bidang Infrastruktur  Jalan Kabupaten Soppeng', 'Kabupaten Soppeng', '2014-06-02', '2014-11-20', '', '2021-12-05 20:33:59', NULL),
(23, 9, 'Paket-05, PengawasanTeknis Jalan Dalam Kota Majene', 'Provinsi Sulawesi Barat', '2013-04-09', '2013-12-01', '', '2021-12-05 20:34:37', NULL),
(24, 9, 'Paket-05, PengawasanTeknis Jalan Mamuju-Tameroddo III', 'Provinsi Sulawesi Barat', '2012-03-23', '2012-10-06', '', '2021-12-05 20:35:11', NULL),
(25, 9, 'Paket-11, Pengawasan Teknis Jalan (Bts. Kabupaten Mamuju – Tameroddo I dan II)', '', '2011-06-01', '2011-10-13', '', '2021-12-05 20:35:37', NULL),
(26, 9, 'Detail Engineering Design (DED) Kegiatan Perencanaan Teknis Peningkatan Jalan Dana Alokasi Khusus(DAK) Kabupaten Majene', '', '2011-03-30', '2011-04-30', '', '2021-12-05 20:36:23', NULL),
(27, 9, 'Perencanaan Teknis Jalan DAK ', 'Kabupaten Gowa', '2010-05-21', '2010-07-20', '', '2021-12-05 20:36:55', NULL),
(28, 9, 'Perencanaan Teknik Jalan (Paket-1)', '', '2010-03-08', '2010-04-06', '', '2021-12-05 20:37:23', NULL),
(29, 9, 'Perencanaan Teknis Kegiatan DAK+DAU Pendamping Sub. Bidang Infrastruktur Jalan Kabupaten Soppeng', 'kab soppeng', '2009-10-01', '2009-12-14', '', '2021-12-05 20:38:09', NULL),
(30, 9, 'Perencanaan Teknik Jalan (Paket-1)', '', '2009-02-10', '2009-03-09', '', '2021-12-05 20:38:34', NULL),
(31, 9, 'Perencanaan Teknik Jalan (ABT) Tersebar di Provinsi Sulawesi Selatan', '', '2008-11-21', '2008-12-14', '', '2021-12-05 20:39:04', NULL),
(32, 9, 'PengawasanTeknik Jalan Rappang – Pinrang Cs (Paket-7)', '', '2008-06-23', '2008-10-29', '', '2021-12-05 20:39:31', NULL),
(33, 9, 'Perencanaan Teknis Kegiatan Pemeliharaan / Peningkatan Jalan dan Jembatan Pedesaan kabupaten Maros ', '', '2008-03-18', '2008-04-23', '', '2021-12-05 20:39:58', NULL),
(34, 9, 'Pembangunan Jalan Palopo Cs', '', '2007-06-10', '2007-12-19', '', '2021-12-05 20:40:31', NULL),
(35, 9, 'Perencanaan Teknis Kegiatan Penataan Lingkungan Permukiman Penduduk Pedesaan (Pembangunan Jalan) Kabupeten Maros', '', '2007-04-19', '2007-06-08', '', '2021-12-05 20:40:55', NULL),
(36, 9, 'PengawasanTeknisPerkerasan danPengaspalanJalan 	Box CulvertKabupatenMamuju', '', '2006-04-01', '2006-10-28', '', '2021-12-05 20:41:17', NULL),
(37, 11, 'Pengawasan Teknis Preservasi JLln. Dolodua – Molibagu – Mamalia - Taludaa (PKT 16)', 'Provinsi Sulawesi Utara Satker P2JN Manado', '2019-03-25', '2019-11-20', '', '2021-12-06 06:20:09', NULL),
(38, 11, 'Core Team Provinsi Gorontalo', 'Provinsi Gorontalo', '2018-03-07', '2018-12-31', '', '2021-12-06 06:21:01', NULL),
(39, 11, '27/Pengawasan Teknik Jembatan Ake Toduku', 'Provinsi Maluku Utara Satker P2JN Maluku Utara', '2017-07-10', '2017-12-06', '', '2021-12-06 06:21:52', NULL),
(40, 11, 'Proyek Pelebaran Jalan Kawangkoan – Woroctan – Poopo (MYC) Kab. Minahasa Selatan', 'Provinsi Sulawesi Utara Satker P2JN Sulawesi Utara', '2015-01-05', '2016-12-20', '', '2021-12-06 06:22:34', NULL),
(41, 11, 'Pengawasan Teknik Pelebaran Jalan Bulontio – Tolinggula (Bts) Prov. Sulteng)', '', '2015-05-23', '2015-12-31', '', '2021-12-06 06:24:12', NULL),
(42, 11, 'Pengawasan Teknis Jalan Isimu Paguyaman II', '', '2014-01-30', '2014-09-23', '', '2021-12-06 06:28:49', NULL),
(43, 11, 'Konsultan Supervisi Paket-21 Pembangunan Jalan Linkar Sumpiuh', '', '2013-04-11', '2013-10-11', '', '2021-12-06 06:29:16', NULL),
(44, 11, 'Supervisi Jalan dan Jembatan Wilayah Lubuk Sikaping – Batas Sumut (Paket 15)', '', '2012-03-23', '2012-11-22', '', '2021-12-06 06:29:52', NULL),
(45, 11, 'Supervisi Contruction Of Highway Sector word Phase III Under IBRD Sector Investment Project', '', '2009-05-31', '2010-03-23', '', '2021-12-06 06:31:34', NULL),
(46, 11, 'Pengawasan Jembatan Dan Jembatan', '', '2008-02-12', '2008-09-15', '', '2021-12-06 06:32:05', NULL),
(47, 11, 'Pengawasan Jalan Program (Pembangunan Siring) ', '', '2011-02-20', '2007-12-17', '', '2021-12-06 06:32:28', NULL),
(48, 11, 'Pengawasan Peningatan Jalan Tabuan – Urin Kab. Balangan', '', '2006-02-10', '2006-08-09', '', '2021-12-06 06:32:57', NULL),
(49, 11, 'Projek Second EastemIndonesia Region Transport project (EIRTP-2)', '', '2002-11-19', '2004-10-09', '', '2021-12-06 06:34:14', NULL),
(50, 11, 'Consulting Servis For Kecamatan Fasilitator of the Kecamatan Development Program (KDP) and Consulting Service for Provincial and Kabupaten Menejement Consultan  of the Kecamatan  Development Program (KDP, IBRD Loan No. 4330-IND)', 'DKI Jakarta', '2001-06-01', '2002-03-31', '', '2021-12-06 06:34:53', NULL),
(51, 11, 'Konsultan Menegemen Teknik', '', '2000-03-10', '2000-09-30', '', '2021-12-06 06:35:43', NULL),
(52, 12, 'Pengawasan Preservasi Rekonstruksi JalanGorontalo – biluhubarat – bilato –    tangkobu', 'prov Gorontalo', '2017-01-30', '2017-12-31', '', '2021-12-06 10:08:36', NULL),
(53, 12, 'Pengawasan Teknis Pelebaran Jalan  Tolanggo - Bulontio', '', '2015-03-13', '2015-11-25', '', '2021-12-06 10:10:10', '2021-12-06 10:10:27'),
(54, 12, 'Pengawasan Teknis JalanKwandang–  Molinggapoto - Isimu', '', '2014-02-04', '2014-09-01', '', '2021-12-06 10:11:49', NULL),
(55, 12, 'Pengawasan Teknis Pelebaran Jalan Batas  Kota Gorontalo – Batas KotaLimboto', '', '2013-03-01', '2013-10-30', '', '2021-12-06 10:12:59', NULL),
(57, 12, 'PENGAWASAN TEKNIS JALAN WILAYAH  PPK 04', '', '2012-10-01', '2012-12-31', '', '2021-12-06 10:36:34', NULL),
(58, 12, 'PENGAWASAN TEKNIS PELAKSANAAN WILAYAH 01', '', '2011-06-01', '2017-12-28', '', '2021-12-06 10:37:05', NULL),
(59, 12, 'PengawasanTeknikJalanLintas Tengah II', '', '2010-04-19', '2010-10-04', '', '2021-12-06 10:37:40', NULL),
(60, 12, 'PENGAWASAN TEKNIS JALAN PRESERVASI IV', '', '2009-05-01', '2009-08-31', '', '2021-12-06 10:38:14', NULL),
(61, 12, 'PengawasanTeknisJalanisimu- Molosipat', '', '2008-05-01', '2008-12-31', '', '2021-12-06 10:38:51', NULL),
(62, 12, 'Pengawasan Teknik Jalan LintasTengah', '', '2007-03-01', '2007-08-31', '', '2021-12-06 10:39:27', NULL),
(63, 12, 'Supervisi Pengawasan Leger Supervisi Pengawasan Jalan Nasional Gunung Menduke', '', '2006-06-10', '2016-11-18', '', '2021-12-06 10:40:28', NULL),
(64, 12, 'pengawasanteknikjalandanjembatatahap I', '', '2005-08-16', '2005-12-15', '', '2021-12-06 10:41:04', NULL),
(65, 12, 'pengawasanteknikjalandanjembatanpulauburu', '', '2004-07-26', '2004-12-07', '', '2021-12-06 10:41:31', NULL),
(66, 12, 'Pengawasan Teknik Talan dan Jembatan Provinsi Nusa Tenggara Timur', '', '2003-03-24', '2003-06-20', '', '2021-12-06 10:42:13', NULL),
(67, 12, 'Pengawasan Jalan 24 ruas D,I Yogyakarta ', '', '2002-03-05', '2002-12-06', '', '2021-12-06 10:42:42', NULL),
(68, 12, 'Pengawasan, pembangunan jalan kabupaten', '', '2001-07-05', '2001-12-17', '', '2021-12-06 10:43:09', NULL),
(69, 12, 'Pengawasan, Pembangunan Jalan dan Drainase ', '', '2000-03-01', '2000-11-30', '', '2021-12-06 10:43:48', NULL),
(70, 12, 'PengawasanJalanDermaga', '', '1998-09-01', '1999-03-10', '', '2021-12-06 10:44:18', NULL),
(71, 10, 'Pengawasan Teknis Preservasi JLln. Dolodua – Molibagu – Mamalia - Taludaa (PKT 16) ', 'Provinsi Sulawesi Utara', '2019-03-25', '2019-11-20', '', '2021-12-06 10:52:12', NULL),
(72, 10, 'Core Team Provinsi Gorontalo', 'Provinsi Gorontalo Satker P2JN Gorontalo', '2018-03-07', '2018-12-31', '', '2021-12-06 10:52:43', NULL),
(73, 10, '27/Pengawasan Teknik Jembatan Ake Toduku', '', '2017-07-10', '2007-12-06', '', '2021-12-06 10:53:14', NULL),
(74, 10, 'Proyek Pelebaran Jalan Kawangkoan – Woroctan – Poopo (MYC) Kab. Minahasa Selatan', '', '2015-01-05', '2016-12-20', '', '2021-12-06 10:53:42', NULL),
(75, 10, 'Pengawasan Teknik Pelebaran Jalan Bulontio – Tolinggula (Bts) Prov. Sulteng) Provinsi Gorontalo', '', '2015-05-23', '2015-12-31', '', '2021-12-06 10:54:15', NULL),
(76, 10, 'Pengawasan Teknis Jalan Isimu Paguyaman II', '', '2014-01-30', '2014-09-23', '', '2021-12-06 10:54:45', NULL),
(77, 10, 'Konsultan Supervisi Paket-21 Pembangunan Jalan Linkar Sumpiuh', '', '2013-04-11', '2013-10-11', '', '2021-12-06 10:55:08', NULL),
(78, 10, 'Supervisi Jalan dan Jembatan Wilayah Lubuk Sikaping – Batas Sumut (Paket 15)', '', '2012-03-22', '2012-11-22', '', '2021-12-06 10:55:38', NULL),
(79, 10, 'Supervisi Jalan dan Jembatan Wilayah Lubuk Sikaping – Batas Sumut (Paket 15)', '', '2012-03-22', '2012-11-22', '', '2021-12-06 10:56:10', NULL),
(80, 10, 'Supervisi Contruction Of Highway Sector word Phase III Under IBRD Sector Investment Project', '', '2009-05-31', '2010-05-23', '', '2021-12-06 10:56:42', NULL),
(81, 10, 'Pengawasan Jembatan Dan Jembatan', '', '2008-02-12', '2008-09-15', '', '2021-12-06 10:57:07', NULL),
(82, 10, 'Pengawasan Jalan Program (Pembangunan Siring) ', 'Prov. Kalimantan Selatan Dinas Pekerjaan Umum  dan Prasarana wilayah', '2011-02-20', '2007-12-17', '', '2021-12-06 10:57:42', NULL),
(83, 10, 'Pengawasan Peningatan Jalan Tabuan – Urin Kab. Balangan', '', '2006-02-10', '2006-08-09', '', '2021-12-06 10:58:09', NULL),
(84, 10, 'Projek Second EastemIndonesia Region Transport project (EIRTP-2)', 'Prov. Papua Barat SNVT P2JJ Prov. Papua Barat', '2002-11-19', '2004-10-09', '', '2021-12-06 10:58:44', NULL),
(85, 10, 'Consulting Servis For Kecamatan Fasilitator of the Kecamatan Development Program (KDP) and Consulting Service for Provincial and Kabupaten Menejement Consultan  of the Kecamatan  Development Program (KDP, IBRD Loan No. 4330-IND)', 'DKI Jakarta', '2001-06-01', '2002-03-31', '', '2021-12-06 10:59:23', NULL),
(86, 10, 'Konsultan Menegemen Teknik', '', '2000-03-10', '2000-09-30', '', '2021-12-06 10:59:54', NULL),
(87, 10, 'Pengawasan Pembangunan/Peningkatan  Landasan', '', '1999-06-19', '1999-11-10', '', '2021-12-06 11:00:28', NULL),
(88, 10, 'Pengawasan Jembatan Dan Jembatan', '', '2008-02-12', '2008-09-15', '', '2021-12-06 11:01:02', NULL),
(89, 10, 'Supervisi Pembuatan Saluran Tersier Sepanjang 25 Km', '', '1993-09-03', '1994-02-03', '', '2021-12-06 11:01:46', NULL),
(90, 10, 'Proyek Peningkatan Jalan Maros Malino – Sepaya Sepanjang 16,5 Km', '', '1994-08-01', '1995-03-28', '', '2021-12-06 11:02:27', NULL),
(91, 10, 'Pengawasan Fasilitas Landasan Bandar Udara Pongkilo', '', '1995-08-11', '1996-01-20', '', '2021-12-06 11:03:01', NULL),
(92, 10, 'Pengawasan Fasilitas Landasan Bandar Udara Pulau Selayar', '', '1997-05-30', '1997-09-29', '', '2021-12-06 11:03:40', NULL),
(93, 10, 'Konsultan Supervisi Proyek Pengembangan Fasilitas Bandar Udara Pulau Selayar', '', '1998-05-15', '1998-10-29', '', '2021-12-06 11:04:26', NULL),
(94, 10, 'Konsultan Menegemen Teknik', '', '2000-03-10', '2000-09-30', '', '2021-12-06 11:05:27', NULL),
(95, 13, 'PENGAWASAN PELAKSANAAN JALAN & JEMBATAN NASIONAL, 						PROVINSI KALIMANTANUTARA ;  PAKET PENGAWASAN TEKNIS 						PEMBANGUNAN  JALAN  MENSALONG  –  TAU  LUMBIS (BUKA HUTAN).', 'Kab. Nunukan, Provinsi  Kalimantan Utara, Kementerian Pekerjaan Umum, Direktorat Jenderal Bina Marga.', '2017-02-01', '2017-12-31', '', '2021-12-06 11:16:26', NULL),
(96, 13, 'PENGAWASAN PELAKSANAAN JALAN & JEMBATAN NASIONAL, 						PROVINSI SULAWESI UTARA ;  PAKET PELEBARAN JALAN KAWANGKOAN – WAROTICAN – POOPO (MYC)', '', '2016-01-01', '2016-12-31', '', '2021-12-06 11:16:58', NULL),
(97, 13, 'PENGAWASAN TEKNIS PEMELIHARAAN PERIODIK DAN PELE - BARAN JALAN (PAKET 10)  PROVINSI SULAWESI TENGGARA', '', '2015-05-02', '2015-12-31', '', '2021-12-06 11:17:35', NULL),
(98, 13, 'KONSULTAN CORE TEAM PERENCANAAN DAN PENGAWASAN  						JALAN DAN JEMBATAN NASIONAL (P2JN) PAKET:12 PROVINSI KEPULAUAN RIAU.', '', '2014-06-01', '2014-12-31', '', '2021-12-06 11:18:15', NULL),
(99, 13, 'EASTERN  INDONESIA  NATIONAL  ROADS  AND  BRIDGES 						IMPROVEMENT PROJECT  (EINRIP – LOAN AIPRD L002, AusAID), PACKAGE: ESH-01  LAKUAN – BUOL , SULAWESI TENGAH.', 'Buol,  ProvinsiSulawesiTengah, Kementerian Pekerjaan  Umum, Direktorat Jenderal Bina Marga.', '2013-01-01', '2014-05-05', '', '2021-12-06 11:19:10', NULL),
(100, 13, 'PENGAWASAN PELAKSANAAN  JALAN & JEMBATAN NASIONAL 						METROPOLITAN JAKARTA ; PAKET PELEBARAN JALAN ABDULAH BIN NUH', '', '2012-04-01', '2012-11-30', '', '2021-12-06 11:19:50', NULL),
(101, 13, 'ACEH ROAD / BRIDGE RECONSTRUCTION AND REHABILITATION PROJECT (NATIONAL ROAD BANDA ACEH – CALANG),', '', '2007-11-01', '2011-11-30', '', '2021-12-06 11:20:28', NULL),
(102, 13, 'PROYEK  REHABILITASI  DAN  REKONSTRUKSI  JALAN  DAN JEMBATAN,  PAKET : BRR – PW 03C', '', '2006-09-01', '2007-02-20', '', '2021-12-06 11:22:33', NULL),
(103, 13, 'PENGAWASAN  PELAKSANAAN  PROYEK  TRANSPORTASI 						DAERAH TIMUR INDONESIA  (EIRTP-1  IBRD LOAN No.4643-IND) 						PAKET : EIP-50  PAL.IV – KM 70', '', '2005-12-01', '2006-06-28', '', '2021-12-06 11:23:02', NULL),
(104, 13, 'PENGAWASAN  PROYEK  PEMBANGUNAN  JEMBATAN  JAWA - BARAT,  PAKET JEMBATAN CIKUBANG  CS  TAHAP  II.', '', '2005-08-01', '2005-12-31', '', '2021-12-06 11:23:54', NULL),
(105, 13, 'PENGAWASAN  TEKNIS  SUMATERA  REGION  ROAD  PROJECT (SRRP IBRD LOAN NO.4307 – IND). JEMBATAN  LINTAS  TIMUR  I(IBRD LOAN NO.4307 – IND), PAKET :LIMA PULUH – SEI BEJANGKAR – KISARAN.', '', '2001-07-01', '2002-12-31', '', '2021-12-06 11:30:31', NULL),
(106, 13, 'PEKERJAAN  KAJIAN  PENGEMBANGAN  KEBIJAKAN 						PEMELIHARAAN  JALAN  DENGAN  SISTEM  KONTRAK  JANGKA PANJANG,  PAKET-13', '', '2000-07-01', '2000-12-31', '', '2021-12-06 11:30:58', NULL),
(107, 13, 'PROYEK BANTUAN PEMBANGUNAN PRASARANA PENDUKUNG DESA TERTINGGAL  (P3DT – OECF PROJECT)', '', '1999-05-01', '2000-05-29', '', '2021-12-06 11:31:26', NULL),
(108, 13, 'PROYEK BANTUAN PEMBANGUNAN PRASARANA PENDUKUNG DESA TERTINGGAL  (P3DT – OECF PROJECT)', '', '1998-04-01', '1999-04-20', '', '2021-12-06 11:31:59', NULL),
(109, 13, 'KONSULTAN  PENGAWASAN  DAN  BANTUAN  TEKNIS  UNTUK PROYEK  JALAN  DAN  JEMBATAN  KABUPATEN  (IBRD – KR5)', '', '1994-08-01', '1998-04-06', '', '2021-12-06 11:32:30', NULL),
(110, 14, 'PW-13. Pengawasan Teknis Jl. dan Jbt. Bts. Sumsel - Sp. Empat - Bukit Kemuning Provinsi Lampung', 'Satker P2JN Lampung', '2019-03-14', '2019-11-30', '', '2021-12-07 18:00:44', NULL),
(111, 14, 'a.	Nama Proyek	:	Pengawasan Preservasi Rehabilitasi mayor Jalan Tolango Paguyaman – Tabulo Marisa  b.	Lokasi Proyek	:	Provinsi Gorontalo', 'Satker P2JN Gorontalo', '2017-02-01', '2017-05-31', '', '2021-12-07 18:01:29', NULL),
(112, 14, 'Pelebaran Jalan Kawangkoang – Worotican – Poopo (MYC) b.	Lokasi Proyek	:	Kab. Minahasa Selatan Prov. Sulut', 'Balai Pelaksana Jalan Nasional XV Sulut dan Gorontalo', '2016-01-05', '2016-12-20', '', '2021-12-07 18:02:10', NULL),
(113, 14, 'Paket – II Pengawasan Teknis Jalan Bts Sulteng –Pasangkayu Provinsi Sulawesi Barat', 'Depertemen Pekerjaan Umum Dirjen Bina Marga', '2013-03-23', '2013-01-27', '', '2021-12-07 18:03:01', NULL),
(114, 14, 'Pengawasan Teknis Jalan dan Jembatan Ruas Enrekang Bts Kab. Sidrap Cs', 'c.	Pengguna Jasa	:	Direktorat Jenderal Bina Marga Balai Pelaksanaan Jalan Nasional VI Makassar', '2012-07-01', '2012-11-07', '', '2021-12-07 18:03:41', NULL),
(115, 14, 'Pengawasan Teknis Jalan Kabupaten Jaya Wijaya II', 'MargDiDirektorat Jenderal Bina Marga ', '2011-03-25', '2011-12-23', '', '2021-12-07 18:04:16', NULL),
(116, 14, 'Pekerjaan lanjutan Pembangunan Fasilitas Pelabuhan Laut Garongkong – Sulawesi Selatan', 'Direktorat Jenderal Perhubungab Laut', '2009-07-06', '2010-01-01', '', '2021-12-07 18:04:54', NULL),
(117, 14, 'Pengawasan Teknis Jembatan Benua Anyar Cs', 'Dinas Pekerjaan Umum', '2009-06-01', '2009-12-29', '', '2021-12-07 18:05:34', NULL),
(118, 14, 'Pengwasan Jalan & Jembatan Paket -18', 'Dinas Pekerjaan Umum', '2008-01-09', '2008-04-09', '', '2021-12-07 18:06:03', NULL),
(119, 14, 'Pengawasan Teknis Kegiatan Pembangunan Jalan', 'Dinas Pekerjaan Umum', '2007-02-22', '2007-12-20', '', '2021-12-07 18:06:39', NULL),
(120, 14, 'Pembangunan Jalan Sp. Blush – Batas Kalteng', 'Ditjen Bina Marga', '2006-04-12', '2006-10-30', '', '2021-12-07 18:07:19', NULL),
(121, 14, 'Pembangunan Jalan Sp. Blush – Batas Kalteng', 'Ditjen Bina Marga', '2006-04-12', '2006-10-30', '', '2021-12-07 18:08:01', NULL),
(122, 14, 'Pengawasan Teknis Pembangunan Jalan Sungguminasa - Takalar', 'Ditjen Bina Marga', '2005-07-15', '2005-12-15', '', '2021-12-07 18:08:39', NULL),
(123, 14, 'Pengawasan Teknik Jalan Dan Jembatan Kabupaten Sleman', 'Dinas Permukiman Dan Prasarana Wilayah', '2004-03-27', '2004-12-04', '', '2021-12-07 18:09:36', NULL),
(124, 14, 'Pengawasan Teknik Jalan Reneren', 'SNVT P2JJ Prov. Nusa Tenggara Timur', '2002-03-07', '2003-08-10', '', '2021-12-07 18:10:18', NULL),
(125, 14, 'Paket-05 Penggawasan Teknik Jalan dan Jembatan Manokhwari', 'Departemen Permukimn Prasarana Wilyah', '2001-07-01', '2001-12-31', '', '2021-12-07 18:10:55', NULL),
(126, 14, 'Pengawasan Jalan Kusuma Bangsa Kalimantan Timur', 'Bagian Proyek Perencanaan dan Pengawasan Teknik Jalan Nasional Prop. Nusa Tenggara Barat', '1999-04-12', '2000-07-10', '', '2021-12-07 18:11:29', NULL),
(127, 14, 'Pengawasan Prasarana Transportasi Jalan Kewilyahan', 'Pengawasan Prasarana Transportasi Jalan Kewilyahan', '1998-05-23', '1998-12-23', '', '2021-12-07 18:12:17', NULL),
(128, 15, 'Penggantian Jembatan S. Bakutaru I, Cs.', 'Pejabat Pembuat Komitmen ( Koridor Awunio – Tobimeita – Wua Wua – Kendari – Airport Haluoleo)', '2017-01-27', '2017-11-11', '', '2021-12-07 18:40:18', NULL),
(129, 15, 'Pengawasan Teknis Pelebaran Jalan Tabulo-Marisa', 'Satker Perencanaan dan Pengawasan Jalan dan Jembatan Povinsi Gorontalo', '2015-03-19', '2015-11-13', '', '2021-12-07 18:44:06', NULL),
(130, 15, 'Pengawasan Teknik Jalan (MYC)', 'SNVT Pelaksanaan Jalan Nasional Wilayah I Provinsi Maluku ', '2012-06-22', '2014-07-12', '', '2021-12-07 18:47:17', NULL),
(131, 15, 'Pelebaran Jalan Kalukku-Salubatu I', 'Satker Pelaksanaan Jalan Nasional Wilayah II Provinsi Sulawesi Barat', '2011-04-25', '2011-11-25', '', '2021-12-07 18:48:54', NULL),
(132, 15, 'Satker Pelaksanaan Jalan Nasional Wilayah II Provinsi Sulawesi Barat', 'SNVT Perencanaan Dan Pengawasan Jalan Dan Jembatan Prov. Sulawesi Selatan', '2008-07-17', '2010-03-09', '', '2021-12-07 18:49:58', NULL),
(133, 15, 'Proyek Peningkatan Jalan ', 'Kegiatan Peningkatan Jalan Kabupaten Bantaeng ', '2006-07-24', '2006-10-05', '', '2021-12-07 18:50:34', NULL),
(135, 15, 'Pengawasan Teknis Jalan ( Paket 2 )', 'Proyek Perencanaan dan Pengawasan Jalan Dan Jembatan Kalsel', '2005-03-03', '2005-11-20', '', '2021-12-19 20:59:11', NULL),
(136, 15, 'Pembangunan Jembatan Kabupaten Enrekang ', '', '2004-08-23', '2004-12-10', '', '2021-12-19 21:00:34', NULL),
(137, 15, 'Pengawasan Teknis Jalan Paket 9 Tanabe-Sanrego-Palattae Cs', '', '2000-06-11', '2000-12-06', '', '2021-12-19 21:01:25', NULL),
(138, 15, 'Penggantian Jembatan IBRD HSIP-2 S Malango Cs Package HB-541', '', '1998-07-10', '1999-07-22', '', '2021-12-19 21:02:22', NULL),
(139, 16, 'Pengawasan tekhnis Pembangunan Jalan Timika – Waghete Provinsi Papua', 'PKK 27 Wilayah VI Provinsi Papua (Timika)', '2016-03-27', '2016-09-17', '', '2021-12-19 21:35:32', NULL),
(140, 16, 'Pengawasan Teknis Pembangunan dan Peningkatan Struktur Jalan ', 'PPK 06 Wil. II Satuan Kerja P2JN Provinsi Papua Barat (Kab.Sorong)', '2015-10-01', '2015-12-02', '', '2021-12-19 21:36:12', NULL),
(141, 16, 'Pengawasan Jalan dan Jembatan Ruas Nasional dan Strategis Bintuni 4', 'PPK 13Wil. IVSatuan Kerja P2JN Provinsi Papua Barat', '2015-05-07', '2015-09-07', '', '2021-12-19 21:37:02', NULL),
(142, 16, '1. Peningkatan Struktur Jalan  Arso Waris. 2. Pemeliharaan berkala jembatan Kali Boom Cs', 'Satker Pelaksanaan Jalan Nasional Wil. I Provinsi Papua (Jayapura)', '2014-05-02', '2014-10-30', '', '2021-12-19 21:37:38', NULL),
(143, 16, '1.Pembangunan jembatan Sungai Hewa 2.Pemb.jembatan Sungai Teppo cs/Koloy', 'SNVT Pelaksanaan Jalan Nasional Wil. II Provinsi Papua (Merauke)', '2012-02-08', '2012-10-04', '', '2021-12-19 21:38:20', NULL),
(144, 16, 'Pemb.Jalan ruas Maros -Pangkajene', 'Pemb.Jalan ruas Maros -Pangkajene', '2011-05-16', '2011-12-26', '', '2021-12-19 21:38:46', NULL),
(145, 16, 'Rehabilitasi dan Rekonnstruksi Pasca Bencana Alam. Pemb.jemb 1.Sungai Tombolo 2.Sungai Bonto Lempangan 3. Sungai Manrimisi', 'Dinas PU Kabupaten Sulawesi - Selatan', '2010-05-27', '2018-12-30', '', '2021-12-19 21:39:40', NULL),
(146, 16, 'Pemb.Jalan Maros - Pangkep', 'SNVT P2JJ Provinsi Sulawesi Selatan', '2009-04-06', '2010-03-09', '', '2021-12-19 21:40:12', NULL),
(147, 16, 'Pemb.6 ruas jalan di Kab. Barru ,Sul Sel', 'Dinas Prasarana Wilayah Kabupaten Barru', '2008-07-09', '2006-12-30', '', '2021-12-19 21:40:43', NULL),
(148, 16, 'Pengawasan Peningkatan Jalan Selo. Taurung', 'SNVT jalan Dan jembatan Kalimantan Selatan', '2006-05-15', '2006-11-15', '', '2021-12-19 21:41:18', NULL),
(149, 16, 'Peningkatan Jaan Di koyta Jayapura', 'Dinas Pekerjaan Umum Kota  Jayapura', '2002-07-01', '2002-11-30', '', '2021-12-19 21:41:50', NULL),
(150, 16, 'Pemb. Jalan dan Jembatan Proyek Dam Bili-Bili', 'SNVT PPSA Jeneberang Dirjen Sumber Daya Air   DEP. PU', '2001-04-11', '2001-12-30', '', '2021-12-19 21:42:20', NULL),
(151, 16, 'Proyek Peningkatan Jalan Sorong', '', '2001-04-11', '2001-12-30', '', '2021-12-19 21:43:02', NULL),
(152, 16, 'KREI Project', 'Pimpro PBPJK Provinsi Sulawesi Selatan', '1995-04-01', '1997-03-31', '', '2021-12-19 21:43:35', NULL),
(153, 16, 'KREI Project', 'Kabupaten Bantaeng – Sulawesi Selatan, Pimpro PBPJK Provinsi Sulawesi Selatan', '1994-08-03', '1995-03-31', '', '2021-12-19 21:44:57', NULL),
(154, 16, 'KREI Project', '', '1993-08-03', '1994-03-31', '', '2021-12-19 21:45:23', NULL),
(155, 16, 'Peningkatanjalan Tinanggea-Kassiputeh', 'RBO Ujung Pandang', '1992-01-02', '1993-03-13', '', '2021-12-19 21:45:53', NULL),
(156, 17, 'Perencanaan Jalan Sugapa – Beoga – Ilaga', 'Satker P2JN Provinsi Papua', '2019-04-26', '2019-12-21', '', '2021-12-19 21:59:33', NULL),
(157, 17, 'Perencanaan Ruas Jalan Tebak Monok – Sp. Waim Embong Ijuk', '', '2018-09-05', '2018-12-04', '', '2021-12-20 08:21:29', NULL),
(158, 17, 'PR-02/2018 Perencanaan Teknik Jalan Ciawi-Puncak-Cianjur', '', '2018-01-29', '2018-07-28', '', '2021-12-20 08:22:06', NULL),
(159, 17, 'PR-02/2017 Perencanaan Teknik Jalan dan Jembatan Metropolitan Jakarta', '', '2017-04-01', '2017-12-06', '', '2021-12-20 08:23:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ska`
--

CREATE TABLE `ska` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `namaSertifikat` enum('MUDA','MADYA','UTAMA') DEFAULT NULL,
  `fileSertifikat` varchar(255) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `updatedAt` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ska`
--

INSERT INTO `ska` (`id`, `userId`, `namaSertifikat`, `fileSertifikat`, `createdAt`, `updatedAt`) VALUES
(1, 7, 'MADYA', '2021-12-05T09-57-08.371Zhal 9.jpg', '2021-12-05 09:57:08', NULL),
(4, 8, 'MADYA', '2021-12-05T19-31-16.682Zsa madya nasri.png', '2021-12-05 19:31:16', NULL),
(5, 8, 'MADYA', '2021-12-05T19-31-28.097Zska nasri 2.png', '2021-12-05 19:31:28', NULL),
(6, 8, 'MADYA', '2021-12-05T19-31-39.254Zska jembatan nasri.png', '2021-12-05 19:31:39', NULL),
(7, 8, 'MADYA', '2021-12-05T19-31-50.607Zska jembatan nasri 2.png', '2021-12-05 19:31:50', NULL),
(8, 9, 'MADYA', '2021-12-05T20-27-26.708Zska madya teknik jalan.png', '2021-12-05 20:27:26', NULL),
(9, 9, 'MADYA', '2021-12-05T20-27-39.364Zska maadya 2.png', '2021-12-05 20:27:39', NULL),
(10, 9, 'MADYA', '2021-12-05T20-27-47.402Zska teknik jembatan.png', '2021-12-05 20:27:47', NULL),
(11, 9, 'MADYA', '2021-12-05T20-27-54.816Zska tenik jembatan 2.png', '2021-12-05 20:27:54', NULL),
(12, 11, 'MADYA', '2021-12-06T06-17-36.511Zska madya teknik jalan.png', '2021-12-06 06:17:36', NULL),
(13, 11, 'MADYA', '2021-12-06T06-17-46.595Zska teknik jalan madya 2.png', '2021-12-06 06:17:46', NULL),
(14, 11, 'MADYA', '2021-12-06T06-18-04.619Zska teknik jembatan madya.png', '2021-12-06 06:18:04', NULL),
(15, 11, 'MADYA', '2021-12-06T06-18-12.858Zska madya jembatan 2.png', '2021-12-06 06:18:12', NULL),
(16, 12, 'MADYA', '2021-12-06T07-12-27.179Zska madya jalan.png', '2021-12-06 07:12:27', NULL),
(17, 12, 'MADYA', '2021-12-06T07-12-36.727Zska madya jalan 2.png', '2021-12-06 07:12:36', NULL),
(18, 12, 'MADYA', '2021-12-06T07-12-44.628Zska jembatan.png', '2021-12-06 07:12:44', NULL),
(19, 12, 'MADYA', '2021-12-06T07-12-54.846Zjembatan 2.png', '2021-12-06 07:12:54', NULL),
(21, 12, 'MADYA', '2021-12-06T07-13-06.361Zk3.png', '2021-12-06 07:13:06', NULL),
(22, 12, 'MADYA', '2021-12-06T07-13-13.267Zk3 2.png', '2021-12-06 07:13:13', NULL),
(23, 10, 'MADYA', '2021-12-06T10-50-02.776Zska madya teknik jalan.png', '2021-12-06 10:50:02', NULL),
(24, 10, 'MADYA', '2021-12-06T10-50-15.155Zska teknik jalan madya 2.png', '2021-12-06 10:50:15', NULL),
(25, 10, 'MADYA', '2021-12-06T10-50-26.720Zska teknik jembatan madya.png', '2021-12-06 10:50:26', NULL),
(26, 10, 'MADYA', '2021-12-06T10-50-35.766Zska madya jembatan 2.png', '2021-12-06 10:50:35', NULL),
(27, 10, 'MADYA', '2021-12-06T10-50-43.944Zska k3.png', '2021-12-06 10:50:44', NULL),
(28, 13, 'MADYA', '2021-12-06T11-13-01.383Zska madya teknik jalan.png', '2021-12-06 11:13:01', NULL),
(29, 13, 'MADYA', '2021-12-06T11-13-13.441Zska madya jalan.png', '2021-12-06 11:13:13', NULL),
(30, 14, 'MADYA', '2021-12-07T17-58-44.838Zska jalan.png', '2021-12-07 17:58:44', NULL),
(31, 14, 'MADYA', '2021-12-07T17-58-58.815Zska jalan 2.png', '2021-12-07 17:58:58', NULL),
(32, 14, 'MADYA', '2021-12-07T17-59-07.857Zska jembatan.png', '2021-12-07 17:59:08', NULL),
(33, 14, 'MADYA', '2021-12-07T17-59-15.684Zska jembatan 2.png', '2021-12-07 17:59:15', NULL),
(34, 15, 'MADYA', '2021-12-07T18-19-15.939Zska teknik jalan.png', '2021-12-07 18:19:15', NULL),
(36, 15, 'MADYA', '2021-12-07T18-38-11.035Zska teknik jalan 2.png', '2021-12-07 18:38:11', NULL),
(37, 15, 'MADYA', '2021-12-07T18-38-20.978Zska jembatan.png', '2021-12-07 18:38:21', NULL),
(38, 15, 'MADYA', '2021-12-07T18-38-30.008Zska jembatan 2.png', '2021-12-07 18:38:30', NULL),
(39, 16, 'MADYA', '2021-12-19T21-32-34.813Zska madya teknik jalan.png', '2021-12-19 21:32:34', NULL),
(40, 16, 'MADYA', '2021-12-19T21-32-40.668Zska madya tenik jalan 2.png', '2021-12-19 21:32:40', NULL),
(41, 16, 'MADYA', '2021-12-19T21-32-45.141Zska madya jembatan.png', '2021-12-19 21:32:45', NULL),
(42, 16, 'MADYA', '2021-12-19T21-32-50.066Zska madya jembatan 2.png', '2021-12-19 21:32:50', NULL),
(43, 17, 'MUDA', '2021-12-19T21-58-24.187Zska muda.png', '2021-12-19 21:58:24', NULL),
(44, 17, 'MUDA', '2021-12-19T21-58-38.676Zska muda 2.png', '2021-12-19 21:58:38', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tahaptender`
--

CREATE TABLE `tahaptender` (
  `id` int(11) NOT NULL,
  `tenderId` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `fileTahap` varchar(255) NOT NULL,
  `tahapMulai` date DEFAULT NULL,
  `tahapSelesai` date DEFAULT NULL,
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `updatedAt` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tahaptender`
--

INSERT INTO `tahaptender` (`id`, `tenderId`, `nama`, `fileTahap`, `tahapMulai`, `tahapSelesai`, `createdAt`, `updatedAt`) VALUES
(23, 33, 'pengumuman prakualifikasi', '6982021-12-27T15-30-24.114Zpakat 1.jpeg', '2021-12-27', '2022-02-14', '2021-12-27 15:30:24', NULL),
(24, 34, 'pengumuman prakualifikasi', '6982021-12-27T15-38-18.229Zpakat 1.jpeg', '2011-11-20', '2022-02-05', '2021-12-27 15:38:18', NULL),
(25, 35, 'prakualifikasi', '6982021-12-27T15-48-29.902Zpakat 1.jpeg', '2021-11-10', '2022-02-04', '2021-12-27 15:48:29', NULL),
(26, 36, 'prakualifikasi', '6982021-12-27T15-55-14.473Zpakat 1.jpeg', '2021-11-12', '2022-02-02', '2021-12-27 15:55:14', NULL),
(27, 37, 'prakualifikasi', '6982021-12-27T16-02-36.629Zpakat 1.jpeg', '2021-12-10', '2022-02-03', '2021-12-27 16:02:36', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tender`
--

CREATE TABLE `tender` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `jabatan` varchar(255) NOT NULL,
  `tahapTenderId` int(11) DEFAULT NULL,
  `instansi` varchar(150) DEFAULT NULL,
  `satuanKerja` varchar(150) DEFAULT NULL,
  `kategori` varchar(150) DEFAULT NULL,
  `nilaiPaguPaket` varchar(100) DEFAULT NULL,
  `provinsi` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL,
  `isActive` enum('true','false') NOT NULL DEFAULT 'true',
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `updatedAt` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tender`
--

INSERT INTO `tender` (`id`, `kode`, `nama`, `jabatan`, `tahapTenderId`, `instansi`, `satuanKerja`, `kategori`, `nilaiPaguPaket`, `provinsi`, `deskripsi`, `isActive`, `createdAt`, `updatedAt`) VALUES
(33, '70322763', 'core team desain dan supervisi', 'QE,SE', 23, '', 'perencanaan dan pengawasan jalan nasional provinsi banten', 'Core Team', 'Rp. 4.500.000.000,00', 'Sulawesi Selatan', '<p>QE :&nbsp;</p><ul><li>a</li><li>s</li><li>d</li></ul><p>SE :</p><ul><li>d</li><li>s</li><li>a</li></ul>', 'true', '2021-12-27 15:25:41', '2022-07-10 08:15:33'),
(34, '49071625', 'pengawasan teknis preservasi cilegon - anyer - pasuruan - sp labuhan - tanjung lesung - cibaliung dan muara binuageun - cibarenok - bts jabar ', 'SE', 24, '', 'perencanaan dan pengawasan jalan nasional provinsi banten ', 'Pengawasan ', 'Rp. 2.163.966.000,00', 'DKI Jakarta', '', 'true', '2021-12-27 15:37:32', '2022-06-25 03:17:32'),
(35, '81002686', 'pengawasan teknis preservasi jalan sp labuhan - saketi - pandeglang - rangkasbitung-cigelung', 'QE', 25, '', 'perencanaan dan pengawasan jalan nasional ', 'pengawasan', 'Rp. 2.196.538.000,00', '', '', 'true', '2021-12-27 15:44:55', '2021-12-27 15:48:31'),
(36, '32498000', 'pengawasan teknis preservasi rehabilitasi jalan subrantas - bts provinsi sumbar (MYC)', 'SE', 26, '', 'P2JN riau', 'pengawasan', 'Rp.  3.126.065.000,00', '', '', 'true', '2021-12-27 15:54:12', '2021-12-27 15:55:16'),
(37, '30639821', 'pengawasan teknik preservasi rehabilitasi jalan sudirman - ma. lembu (MYC)', 'QE', 27, '', 'P2JN Riau', 'pengawasan', 'Rp. 6.974.664.000,00', '', '', 'true', '2021-12-27 16:00:45', '2021-12-27 16:02:38');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `tempatLahir` varchar(255) DEFAULT NULL,
  `tanggalLahir` date DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `provinsi` varchar(255) NOT NULL,
  `kabupaten` varchar(255) DEFAULT NULL,
  `kodePos` varchar(255) DEFAULT NULL,
  `noTelp` varchar(255) DEFAULT NULL,
  `pendidikanTerakhir` varchar(255) DEFAULT NULL,
  `jurusan` varchar(255) DEFAULT NULL,
  `fileIjazahTerakhir` varchar(255) DEFAULT NULL,
  `gaji` varchar(255) DEFAULT NULL,
  `nik` varchar(20) DEFAULT NULL,
  `scanKtp` varchar(255) DEFAULT NULL,
  `ska` enum('MUDA','MADYA','UTAMA') DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` enum('admin','user') NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `updatedAt` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama`, `tempatLahir`, `tanggalLahir`, `alamat`, `provinsi`, `kabupaten`, `kodePos`, `noTelp`, `pendidikanTerakhir`, `jurusan`, `fileIjazahTerakhir`, `gaji`, `nik`, `scanKtp`, `ska`, `email`, `password`, `role`, `createdAt`, `updatedAt`) VALUES
(3, 'Admin', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'admin@gmail.com', '$2b$10$y86cpFx.0wv1hAPzjw4tk.lc1OYE3NdeQMKMf6qFnRpueIlFSwV/u', 'admin', '2021-09-16 13:47:50', '2021-09-25 10:15:24'),
(5, 'user', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'user@gmail.com', '$2b$10$y86cpFx.0wv1hAPzjw4tk.lc1OYE3NdeQMKMf6qFnRpueIlFSwV/u', 'user', '2021-10-23 08:23:30', '2022-06-23 16:33:41'),
(8, 'Ir. Nasri Chairun', 'Ujung Pandang', '1988-12-31', 'Jl. Malengkere 3 Blok A no 09 ', 'Gorontalo', 'Gorontalo', '15138', '081237890173', 'S1', 'Teknik Sipil ', '2021-12-05T19-20-17.390Zijazah nasri.png', '1000000', '7371110108640001', '2021-12-05T19-20-17.520Zktp nasri.png', 'MADYA', 'rzkyramadhanykasim@gmail.com', '$2b$10$y86cpFx.0wv1hAPzjw4tk.lc1OYE3NdeQMKMf6qFnRpueIlFSwV/u', 'user', '2021-12-05 19:16:47', '2022-06-23 13:02:46'),
(9, 'Ir. Nuraeni', 'Rappang', '1990-06-12', 'Jl. Persada Indah No. 12 Kel. Bumi Harapan ', 'Kalimantan Selatan', 'pare-pare', '30123', '081298733455', 'S1', 'Teknik Sipil ', '2021-12-05T20-27-03.620Zijazah nuraeni.png', '5000000', '737204471064003', '2021-12-05T20-27-03.627Zktp.png', 'MADYA', 'ilhamrakailhamrak@gmail.com', '$2b$10$y86cpFx.0wv1hAPzjw4tk.lc1OYE3NdeQMKMf6qFnRpueIlFSwV/u', 'user', '2021-12-05 20:24:23', '2022-06-23 15:48:54'),
(10, 'Ir. Nakir Muliadi', 'Gorontalo', '1964-11-02', 'BTP Jl. Kerukunan Barat 8 Blok J/184 Kel. Tamalarea Kec. Tamalrea', 'Sulawesi Selatan', 'makassar', '30123', '08118976346', 'S1', 'Tekni Sipil', '2021-12-06T10-49-44.312Zijazah nakir.png', '7000000', '7371140211640004', '2021-12-06T10-49-44.383Ztp.png', 'MADYA', 'mikhylaslwdyhfh@gmail.com', '$2b$10$y86cpFx.0wv1hAPzjw4tk.lc1OYE3NdeQMKMf6qFnRpueIlFSwV/u', 'user', '2021-12-06 06:14:00', '2022-06-24 00:04:47'),
(11, 'Ir. Nakir Muliadi', 'Gorontalo', '1964-11-02', 'BTP Jl. Kerukunan Barat 8 Blok J/184 Kel. Tamalarea Kec. Tamalrea', 'Sulawesi Selatan', 'makassar', '30123', '081218136488', 'S1', 'Tekni Sipil', '2021-12-06T06-17-13.922Zijazah nakir.png', '3000000', '7371140211640004', '2021-12-06T06-17-14.009Ztp.png', 'MADYA', 'rzkyramadhankasim@gmail.com', '$2b$10$y86cpFx.0wv1hAPzjw4tk.lc1OYE3NdeQMKMf6qFnRpueIlFSwV/u', 'user', '2021-12-06 06:14:29', '2021-12-27 19:49:32'),
(12, 'Muhammad Sahib, ST. ', 'Makassar', '1967-02-27', 'Jl. Satando I', 'Sulawesi Selatan', 'makasar', '30123', '085241423360', 'S1', 'Tekni Sipil', '2021-12-06T07-11-49.911Zijazah.png', '8000000', '7371052702670001', '2021-12-06T07-11-50.006Zktp.png', 'MADYA', 'mkhylslwdyh10@gmail.com', '$2b$10$y86cpFx.0wv1hAPzjw4tk.lc1OYE3NdeQMKMf6qFnRpueIlFSwV/u', 'user', '2021-12-06 07:08:09', '2021-12-26 12:43:19'),
(13, 'Ir. Sugeng Sarwono', 'Semarang', '1957-12-25', 'Jl. Empu Kanwa Raya  No.38,RT/RW:005/07 Kel. Cibodas Baru, Kec. Cibodas Perumnas 2 – Karawaci, Tangerang ', 'DKI Jakarta', 'dki jakarta', '15138', '081360855250', 'S1', 'Tekni Sipil', '2021-12-06T11-12-47.998Zijazah sugeng.png', '10000000', '3671092512570004', '2021-12-06T11-12-48.004Zktp.png', 'MADYA', 'mikhaylasalwaaa@gmail.com', '$2b$10$y86cpFx.0wv1hAPzjw4tk.lc1OYE3NdeQMKMf6qFnRpueIlFSwV/u', 'user', '2021-12-06 11:09:18', '2021-12-26 12:54:08'),
(14, 'Sudirman, ST', 'Kolaka', '1965-05-03', 'BTN Kendari Permai U5 kel. Padele Kambu ', 'Sulawesi Tenggara', 'kota kendari', '30123', '081290887535', 'S1', 'Tekni Sipil', '2021-12-07T17-57-26.395Zijazah.png', '4000000', '7471 1003 8565 0001', '2021-12-07T17-57-26.482Zktp.png', 'MADYA', 'mkhylslwdyh11@gmail.com', '$2b$10$y86cpFx.0wv1hAPzjw4tk.lc1OYE3NdeQMKMf6qFnRpueIlFSwV/u', 'user', '2021-12-07 17:53:08', '2021-12-26 12:38:50'),
(15, 'Erfan Makmur, ST', 'Bulukumba', '1972-06-26', 'Jl. Sultan Hassanudin Kel. Sawitto Kec. Watang Sawitto', 'Sulawesi Selatan', 'Bulukumba', '30123', '0895876573822', 'S1', 'Tekni Sipil', '2021-12-07T18-19-02.220Zijazzah.png', '4500000', '7315 1126 0672 0003', '2021-12-07T18-19-02.279Zktp.png', 'MADYA', 'heybibi01@gmail.com', '$2b$10$y86cpFx.0wv1hAPzjw4tk.lc1OYE3NdeQMKMf6qFnRpueIlFSwV/u', 'user', '2021-12-07 18:16:28', '2021-12-26 12:54:53'),
(16, 'Amris, ST', 'Makassar', '1963-12-25', 'Komp. Perumahan Palu Cipta Nugraha G/9 Kelurahan Turikale Kecamatan Adatongeng, Kab. Maros - Sulawesi Selatan.', 'Sulawesi Selatan', 'makassar', '30123', '081266789001', 'S1', 'Tekni Sipil', '2021-12-19T21-32-21.364Zijazah.png', '6500000', '7309 1425 1263.0005', '2021-12-19T21-32-21.557Zktp.png', 'MADYA', 'mrshunsinee@gmail.com', '$2b$10$y86cpFx.0wv1hAPzjw4tk.lc1OYE3NdeQMKMf6qFnRpueIlFSwV/u', 'user', '2021-12-19 21:30:24', '2021-12-26 12:56:18'),
(17, 'Abd Rasyid ST', 'Turadu', '1982-10-15', 'jl madu 23', 'Sulawesi Selatan', 'makassar', '346799', '081278990112', 'S1', 'Tekni Sipil', '2021-12-20T08-14-43.262Zijzh.png', '7500000', '762131510820002', '2021-12-19T21-58-05.714Zktp.png', 'MUDA', 'cikicitatos01@gmail.com', '$2b$10$y86cpFx.0wv1hAPzjw4tk.lc1OYE3NdeQMKMf6qFnRpueIlFSwV/u', 'user', '2021-12-19 21:51:41', '2021-12-27 19:43:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jointender`
--
ALTER TABLE `jointender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pajak`
--
ALTER TABLE `pajak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengalaman`
--
ALTER TABLE `pengalaman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ska`
--
ALTER TABLE `ska`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tahaptender`
--
ALTER TABLE `tahaptender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tender`
--
ALTER TABLE `tender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jointender`
--
ALTER TABLE `jointender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=173;

--
-- AUTO_INCREMENT for table `pajak`
--
ALTER TABLE `pajak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=124;

--
-- AUTO_INCREMENT for table `pengalaman`
--
ALTER TABLE `pengalaman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=955;

--
-- AUTO_INCREMENT for table `ska`
--
ALTER TABLE `ska`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=193;

--
-- AUTO_INCREMENT for table `tahaptender`
--
ALTER TABLE `tahaptender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- AUTO_INCREMENT for table `tender`
--
ALTER TABLE `tender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=126;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
