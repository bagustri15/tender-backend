function checkAlamat(provinsi, provinsiTender) {
  const provinsi1 = [
    "Aceh",
    "Sumatera Barat",
    "Sumatera Selatan",
    "Sumatera Utara",
    "Riau",
    "Jambi",
    "Bengkulu",
    "Lampung",
    "Kepulauan Bangka Belitung",
    "Kepulauan Riau",
  ];
  const provinsi2 = [
    "Kalimantan Barat",
    "Kalimantan Selatan",
    "Kalimantan Tengah",
    "Kalimantan Timur",
    "Kalimantan Utara",
  ];
  const provinsi3 = [
    "Banten",
    "DKI Jakarta",
    "DKI Yogyakarta",
    "Jawa Barat",
    "Jawa Tengah",
    "Jawa Timur",
  ];
  const provinsi4 = ["Bali", "Nusa Tenggara Barat", "Nusa Tenggara Timur"];
  const provinsi5 = [
    "Gorontalo",
    "Sulawesi Barat",
    "Sulawesi Selatan",
    "Sulawesi Tengah",
    "Sulawesi Tenggara",
    "Sulawesi Utara",
  ];
  const provinsi6 = ["Maluku", "Maluku Utara"];
  const provinsi7 = ["Papua", "Papua Barat"];

  if (provinsi === provinsiTender) {
    return "DEKAT";
  }

  let check = [];
  check = provinsi1.includes(provinsiTender)
    ? provinsi1
    : provinsi2.includes(provinsiTender)
    ? provinsi2
    : provinsi3.includes(provinsiTender)
    ? provinsi3
    : provinsi4.includes(provinsiTender)
    ? provinsi4
    : provinsi5.includes(provinsiTender)
    ? provinsi5
    : provinsi6.includes(provinsiTender)
    ? provinsi6
    : provinsi7.includes(provinsiTender)
    ? provinsi7
    : [];

  if (check.includes(provinsi)) {
    return "SEDANG";
  }

  return "JAUH";
}

module.exports = checkAlamat;
