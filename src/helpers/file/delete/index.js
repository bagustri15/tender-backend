const fs = require("fs");

const deleteFileUser = (fileName) => {
  if (fs.existsSync(`public/uploads/user/${fileName}`)) {
    fs.unlink(`public/uploads/user/${fileName}`, (error) => {
      if (error) throw error;
    });
  }
};

const deleteFilePajak = (fileName) => {
  if (fs.existsSync(`public/uploads/pajak/${fileName}`)) {
    fs.unlink(`public/uploads/pajak/${fileName}`, (error) => {
      if (error) throw error;
    });
  }
};

const deleteFileSka = (fileName) => {
  if (fs.existsSync(`public/uploads/ska/${fileName}`)) {
    fs.unlink(`public/uploads/ska/${fileName}`, (error) => {
      if (error) throw error;
    });
  }
};

const deleteFileTender = (fileName) => {
  if (fs.existsSync(`public/uploads/tender/${fileName}`)) {
    fs.unlink(`public/uploads/tender/${fileName}`, (error) => {
      if (error) throw error;
    });
  }
};

const deleteFilePengalaman = (fileName) => {
  if (fs.existsSync(`public/uploads/pengalaman/${fileName}`)) {
    fs.unlink(`public/uploads/pengalaman/${fileName}`, (error) => {
      if (error) throw error;
    });
  }
};

module.exports = {
  deleteFileUser,
  deleteFilePajak,
  deleteFileSka,
  deleteFileTender,
  deleteFilePengalaman,
};
