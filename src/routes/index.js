const express = require("express");

const Route = express.Router();

const dataTrainingRouter = require("../modules/dataTraining/dataTrainingRoutes");
const authRouter = require("../modules/auth/authRoutes");
const userRouter = require("../modules/user/userRoutes");
const tenderRouter = require("../modules/tender/tenderRoutes");
const joinTenderRouter = require("../modules/joinTender/joinTenderRoutes");
const tahapTenderRouter = require("../modules/tahapTender/tahapTenderRoutes");
const exportRouter = require("../modules/export/exportRoutes");

Route.use("/data-training", dataTrainingRouter);
Route.use("/auth", authRouter);
Route.use("/user", userRouter);
Route.use("/tender", tenderRouter);
Route.use("/join-tender", joinTenderRouter);
Route.use("/tahap-tender", tahapTenderRouter);
Route.use("/export", exportRouter);

module.exports = Route;
