const express = require("express");

require("dotenv").config();

const morgan = require("morgan");
const cors = require("cors");
const bodyParser = require("body-parser");
const routerNavigation = require("./routes");

const app = express();
const port = process.env.PORT || 3001;

app.use(morgan("dev"));
app.use(cors());
app.options("*", cors());
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());
app.use(express.static("public"));
app.use("/", routerNavigation);

// app.get("/hello", (req, res) => {
//   res.send("Hello World!");
// });
app.use("/*", (request, response) => {
  response.status(404).send("Path Not Found!");
});

app.listen(port, () => {
  console.log(`Express app is listen on port ${port} !`);
});
