const helper = require("../../helpers/wrapper");
const authModel = require("./authModel");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

module.exports = {
  login: async (req, res) => {
    try {
      const { email, password } = req.body;
      const checkEmailUser = await authModel.getDataConditions({ email });
      if (checkEmailUser.length < 1) {
        return helper.response(res, 404, "Email not registed !");
      }
      const checkPassword = bcrypt.compareSync(
        password,
        checkEmailUser[0].password
      );

      if (!checkPassword) {
        return helper.response(res, 400, "Wrong password !");
      }

      let payload = checkEmailUser[0];
      delete payload.password;
      const token = jwt.sign({ ...payload }, "RAHASIA");
      const result = { id: payload.id, token };
      return helper.response(res, 200, "Sukses login !", result);
    } catch (error) {
      return helper.response(
        res,
        400,
        `Bad Request${error.message ? " (" + error.message + ")" : ""}`,
        null
      );
    }
  },
  register: async (req, res) => {
    try {
      const { email, password } = req.body;
      const salt = bcrypt.genSaltSync(10);
      const encryptPassword = bcrypt.hashSync(password, salt);

      const checkEmailUser = await authModel.getDataConditions({ email });
      if (checkEmailUser.length > 0) {
        return helper.response(res, 400, "Email has been registed !");
      }

      const setData = {
        ...req.body,
        password: encryptPassword,
        role: "user",
      };

      const result = await authModel.register(setData);
      await authModel.addDataPajak({ userId: result.id });
      delete result.password;
      return helper.response(res, 200, "Sukses daftar user baru", result);
    } catch (error) {
      return helper.response(
        res,
        400,
        `Bad Request${error.message ? " (" + error.message + ")" : ""}`,
        null
      );
    }
  },
};
