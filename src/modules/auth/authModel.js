const connection = require("../../config/mysql");

module.exports = {
  getDataConditions: (data) =>
    new Promise((resolve, reject) => {
      connection.query("SELECT * FROM user WHERE ?", data, (error, result) => {
        !error
          ? resolve(result)
          : reject(new Error(`SQL : ${error.sqlMessage}`));
      });
    }),
  register: (data) =>
    new Promise((resolve, reject) => {
      connection.query("INSERT INTO user SET ?", data, (error, result) => {
        if (!error) {
          const newResult = {
            id: result.insertId,
            ...data,
          };
          resolve(newResult);
        } else {
          reject(new Error(`SQL : ${error.sqlMessage}`));
        }
      });
    }),
  addDataPajak: (data) =>
    new Promise((resolve, reject) => {
      connection.query("INSERT INTO pajak SET ?", data, (error, result) => {
        if (!error) {
          const newResult = {
            id: result.insertId,
            ...data,
          };
          resolve(newResult);
        } else {
          reject(new Error(`SQL : ${error.sqlMessage}`));
        }
      });
    }),
};
