const helper = require("../../helpers/wrapper");
const exportModel = require("./exportModel");
const ejs = require("ejs");
const pdf = require("html-pdf");
const path = require("path");
const students = [
  { name: "Joy", email: "joy@example.com", city: "New York", country: "USA" },
  {
    name: "John",
    email: "John@example.com",
    city: "San Francisco",
    country: "USA",
  },
  {
    name: "Clark",
    email: "Clark@example.com",
    city: "Seattle",
    country: "USA",
  },
  {
    name: "Watson",
    email: "Watson@example.com",
    city: "Boston",
    country: "USA",
  },
  {
    name: "Tony",
    email: "Tony@example.com",
    city: "Los Angels",
    country: "USA",
  },
];

module.exports = {
  exportLisUser: async (req, res) => {
    try {
      const { id } = req.params;
      let result = await exportModel.getListUser(id);
      result = result.map((item, index) => {
        return { no: index + 1, ...item };
      });
      const fileName = `tender-${id}.pdf`;
      ejs.renderFile(
        path.resolve("./src/templates/list.ejs"),
        { result },
        (error, result) => {
          if (error) {
            console.log(error);
          } else {
            const options = {
              height: "11.25in",
              width: "8.5in",
              header: {
                height: "20mm",
              },
              footer: {
                height: "20mm",
              },
            };
            pdf
              .create(result, options)
              .toFile(
                path.resolve(`./public/export/${fileName}`),
                (error, result) => {
                  console.log(result);
                  if (error) {
                    console.log(error);
                  } else {
                    return helper.response(res, 200, "Sukses export data", {
                      url: `http://localhost:3001/export/${fileName}`,
                    });
                  }
                }
              );
          }
        }
      );
    } catch (error) {
      return helper.response(
        res,
        400,
        `Bad Request${error.message ? " (" + error.message + ")" : ""}`,
        null
      );
    }
  },
};
