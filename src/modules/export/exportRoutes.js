const express = require("express");
const Route = express.Router();

const exportController = require("./exportController");

Route.get("/:id", exportController.exportLisUser);

module.exports = Route;
