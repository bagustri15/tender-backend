const connection = require("../../config/mysql");

module.exports = {
  getListUser: (id) =>
    new Promise((resolve, reject) => {
      connection.query(
        "SELECT user.nama AS nama, joinTender.statusSeleksi AS statusSeleksi, joinTender.statusDokumen AS statusDokumen, joinTender.jabatan AS jabatan FROM joinTender JOIN user ON joinTender.userId = user.id WHERE tenderId = ? AND statusDokumen = 'LOLOS'",
        id,
        (error, result) => {
          !error
            ? resolve(result)
            : reject(new Error(`SQL : ${error.sqlMessage}`));
        }
      );
    }),
};
