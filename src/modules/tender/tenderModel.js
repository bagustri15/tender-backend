const connection = require("../../config/mysql");

module.exports = {
  getTenderByCondition: (data) =>
    new Promise((resolve, reject) => {
      connection.query(
        "SELECT * FROM tender WHERE ? ",
        data,
        (error, result) => {
          !error
            ? resolve(result)
            : reject(new Error(`SQL : ${error.sqlMessage}`));
        }
      );
    }),
  getDataCount: (search, isActive) =>
    new Promise((resolve, reject) => {
      connection.query(
        `SELECT COUNT(*) AS total FROM tender WHERE nama LIKE ? ${isActive}`,
        [`%${search}%`],
        (error, result) => {
          !error
            ? resolve(result[0].total)
            : reject(new Error("SQL : " + error.sqlMessage));
        }
      );
    }),
  getDataAll: (search, isActive, limit, offset) =>
    new Promise((resolve, reject) => {
      connection.query(
        `SELECT * FROM tender WHERE nama LIKE ? ${isActive} LIMIT ? OFFSET ?`,
        [`%${search}%`, limit, offset],
        (error, result) => {
          !error
            ? resolve(result)
            : reject(new Error("SQL : " + error.sqlMessage));
        }
      );
    }),
  postTender: (data) =>
    new Promise((resolve, reject) => {
      connection.query("INSERT INTO tender SET ?", data, (error, result) => {
        if (!error) {
          const newResult = {
            id: result.insertId,
            ...data,
          };
          resolve(newResult);
        } else {
          reject(new Error("SQL : " + error.sqlMessage));
        }
      });
    }),
  updateTender: (data, id) =>
    new Promise((resolve, reject) => {
      connection.query(
        "UPDATE tender SET ? WHERE id = ?",
        [data, id],
        (error, result) => {
          if (!error) {
            const newResult = {
              id,
              ...data,
            };
            resolve(newResult);
          } else {
            reject(new Error("SQL : " + error.sqlMessage));
          }
        }
      );
    }),
  deleteTender: (id) =>
    new Promise((resolve, reject) => {
      connection.query(
        "DELETE FROM tender WHERE id = ?",
        id,
        (error, result) => {
          if (!error) {
            resolve(id);
          } else {
            reject(new Error("SQL : " + error.sqlMessage));
          }
        }
      );
    }),
};
