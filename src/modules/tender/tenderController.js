const helper = require("../../helpers/wrapper");
const tenderModel = require("./tenderModel");
const tahapTenderModel = require("../tahapTender/tahapTenderModel");
const userModel = require("../user/userModel");

const nodemailer = require("nodemailer");
const clientId = '425514367559-om4fj0763jl5g3iqaaja6qb204e58lvq.apps.googleusercontent.com';
const clientSecret = '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';
const refreshToken =
  '1//042aX7mhnTsb4CgYIARAAGAQSNwF-L9IrJnv8-gLPlXlC1ssCan4IwFXXs1SFYdtP8I4k-H6fUzxUZo9eRT26S73Nvoa-JgLDHHI';
  
const { google } = require("googleapis");
const OAuth2 = google.auth.OAuth2;
const OAuth2_client = new OAuth2(clientId, clientSecret);
OAuth2_client.setCredentials({
  refresh_token: refreshToken,
});

module.exports = {
  getAllData: async (req, res) => {
    try {
      let { page, limit, search, isActive } = req.query;
      page = page ? parseInt(page) : 1;
      limit = limit ? parseInt(limit) : 10;
      search = search || "";
      isActive = isActive ? "AND isActive = 'true'" : "";

      const totalData = await tenderModel.getDataCount(search, isActive);
      
      const totalPage = Math.ceil(totalData / limit);
      const offset = page * limit - limit;
      const pageInfo = {
        page,
        totalPage,
        limit,
        totalData,
      };
      const result = await tenderModel.getDataAll(
        search,
        isActive,
        limit,
        offset
      );
      for (const data of result) {
        data.listTahap = await tahapTenderModel.getTahapTenderByCondition({
          tenderId: data.id,
        });
      }
      return helper.response(res, 200, "Success Get Data", result, pageInfo);
    } catch (error) {
      return helper.response(
        res,
        400,
        `Bad Request${error.message ? " (" + error.message + ")" : ""}`,
        null
      );
    }
  },
  getDataByIdTender: async (req, res) => {
    try {
      const { id } = req.params;
      const result = await tenderModel.getTenderByCondition({ id });
      if (result.length < 1) {
        return helper.response(res, 404, `Data ${id} tidak ditemukan !`, null);
      }
      result[0].listTahap = await tahapTenderModel.getTahapTenderByCondition({
        tenderId: result[0].id,
      });
      return helper.response(res, 200, "Sukses ambil data", result);
    } catch (error) {
      return helper.response(
        res,
        400,
        `Bad Request${error.message ? " (" + error.message + ")" : ""}`,
        null
      );
    }
  },
  sendMail: async (req, res) => {
    try {
      const { id, status } = req.query;
      const dataUser = await userModel.getDataConditions(id);
      const accessToken = OAuth2_client.getAccessToken;
      const msg =
        status === "LOLOS"
          ? "Selamat Anda LOLOS Tender, Silahkan Penuhi Syarat Yang Terlampir dan Kirim File ke Email ini"
          : "Mohon Maaf Anda TIDAK LOLOS Tender !";

      const transporter = nodemailer.createTransport({
        service: "gmail",
        auth: {
          type: "OAuth2",
          user: "memo.in.aja@gmail.com",
          clientId: clientId,
          clientSecret: clientSecret,
          refreshToken: refreshToken,
          accessToken: accessToken,
        },
      });

      const mailOptions = {
        from: '"PT. Tridacomi Andalan Semesta" <memo.in.aja@gmail.com>', // sender address
        to: dataUser[0].email, // list of receivers
        subject: "Informasi Status Tender", // Subject line
        html: msg,
        attachments: status === "LOLOS" ? [{
          filename: "syarat.pdf",
          path: "syarat.pdf"
        }] : null
      };
      await transporter.sendMail(mailOptions, async (error, info) => {
        if (error) {
          console.log(error);
          return helper.response(res, 400, "Gagal kirim email", null);
        } else {
          return helper.response(res, 200, "Sukses kirim email", dataUser[0]);
        }
      });
    } catch (error) {
      return helper.response(
        res,
        400,
        `Bad Request${error.message ? " (" + error.message + ")" : ""}`,
        null
      );
    }
  },
  addData: async (req, res) => {
    try {
      const randomNumber = Math.floor(
        Math.pow(10, 8 - 1) + Math.random() * 9 * Math.pow(10, 8 - 1)
      );
      const setData = {
        ...req.body,
        kode: randomNumber,
      };
      const result = await tenderModel.postTender(setData);
      return helper.response(res, 200, "Sukses add tender", result);
    } catch (error) {
      return helper.response(
        res,
        400,
        `Bad Request${error.message ? " (" + error.message + ")" : ""}`,
        null
      );
    }
  },
  updateData: async (req, res) => {
    try {
      const { id } = req.params;
      const result = await tenderModel.getTenderByCondition({ id });
      if (result.length < 1) {
        return helper.response(res, 404, `Data ${id} tidak ditemukan !`, null);
      }

      const setData = {
        ...req.body,
      };

      for (const data in setData) {
        if (!setData[data]) {
          delete setData[data];
        }
      }

      await tenderModel.updateTender(
        { ...setData, updatedAt: new Date(Date.now()) },
        id
      );

      return helper.response(res, 200, "Sukses update tender", {
        ...result[0],
        ...setData,
      });
    } catch (error) {
      return helper.response(
        res,
        400,
        `Bad Request${error.message ? " (" + error.message + ")" : ""}`,
        null
      );
    }
  },
  deleteData: async (req, res) => {
    try {
      const { id } = req.params;
      const result = await tenderModel.getTenderByCondition({ id });
      if (result.length < 1) {
        return helper.response(res, 404, `Data ${id} tidak ditemukan !`, null);
      }

      await tenderModel.deleteTender(id);
      return helper.response(res, 200, "Sukses delete tender", result[0]);
    } catch (error) {
      return helper.response(
        res,
        400,
        `Bad Request${error.message ? " (" + error.message + ")" : ""}`,
        null
      );
    }
  },
};
