const express = require("express");
const Route = express.Router();

const tenderController = require("./tenderController");
const uploadHelperTender = require("../../helpers/file/uploads/tender");

Route.get("/", tenderController.getAllData);
Route.get("/:id", tenderController.getDataByIdTender);
Route.get("/sendmail/user", tenderController.sendMail);
Route.post("/", tenderController.addData);
Route.patch("/:id", tenderController.updateData);
Route.delete("/:id", tenderController.deleteData);

module.exports = Route;
