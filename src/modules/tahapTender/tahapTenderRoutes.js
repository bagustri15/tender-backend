const express = require("express");
const Route = express.Router();

const tahapTenderController = require("./tahapTenderController");

const uploadHelperTender = require("../../helpers/file/uploads/tender");

Route.get("/:id", tahapTenderController.getTahapTender);
Route.post("/", uploadHelperTender, tahapTenderController.addTahapTender);
Route.patch(
  "/:id",
  uploadHelperTender,
  tahapTenderController.updateTahapTender
);
Route.delete("/:id", tahapTenderController.deleteTahapTender);

module.exports = Route;
