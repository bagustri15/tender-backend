const helper = require("../../helpers/wrapper");
const tahapTenderModel = require("./tahapTenderModel");
const fileDelete = require("../../helpers/file/delete");

module.exports = {
  getTahapTender: async (req, res) => {
    try {
      const { id } = req.params;
      const result = await tahapTenderModel.getTahapTenderByCondition({
        tenderId: id,
      });
      return helper.response(res, 200, "Sukses get tahap tender", result);
    } catch (error) {
      return helper.response(
        res,
        400,
        `Bad Request${error.message ? " (" + error.message + ")" : ""}`,
        null
      );
    }
  },
  addTahapTender: async (req, res) => {
    try {
      const setData = {
        ...req.body,
        fileTahap: req.file ? req.file.filename : "",
      };
      const result = await tahapTenderModel.postTahapTender(setData);
      return helper.response(res, 200, "Sukses add tahap tender", result);
    } catch (error) {
      return helper.response(
        res,
        400,
        `Bad Request${error.message ? " (" + error.message + ")" : ""}`,
        null
      );
    }
  },
  updateTahapTender: async (req, res) => {
    try {
      const { id } = req.params;
      const result = await tahapTenderModel.getTahapTenderByCondition({ id });
      if (result.length < 1) {
        return helper.response(res, 404, `Data ${id} tidak ditemukan !`, null);
      }

      const setData = {
        ...req.body,
        fileTahap: req.file ? req.file.filename : "",
      };

      !setData.fileTahap
        ? delete setData.fileTahap
        : fileDelete.deleteFileTender(result[0].fileTahap);

      await tahapTenderModel.updateTahapTender(
        { ...setData, updatedAt: new Date(Date.now()) },
        id
      );

      return helper.response(res, 200, "Sukses update tahap tender", {
        ...result[0],
        ...setData,
      });
    } catch (error) {
      return helper.response(
        res,
        400,
        `Bad Request${error.message ? " (" + error.message + ")" : ""}`,
        null
      );
    }
  },
  deleteTahapTender: async (req, res) => {
    try {
      const { id } = req.params;
      const result = await tahapTenderModel.getTahapTenderByCondition({ id });
      if (result.length < 1) {
        return helper.response(res, 404, `Data ${id} tidak ditemukan !`, null);
      }
      fileDelete.deleteFileTender(result[0].fileTahap);
      await tahapTenderModel.deleteTahapTender(id);
      return helper.response(res, 200, "Sukses delete tahap tender", result[0]);
    } catch (error) {
      return helper.response(
        res,
        400,
        `Bad Request${error.message ? " (" + error.message + ")" : ""}`,
        null
      );
    }
  },
};
