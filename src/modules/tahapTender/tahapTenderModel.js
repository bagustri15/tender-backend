const connection = require("../../config/mysql");

module.exports = {
  getTahapTenderByCondition: (data) =>
    new Promise((resolve, reject) => {
      connection.query(
        "SELECT * FROM tahaptender WHERE ? ",
        data,
        (error, result) => {
          !error
            ? resolve(result)
            : reject(new Error(`SQL : ${error.sqlMessage}`));
        }
      );
    }),
  postTahapTender: (data) =>
    new Promise((resolve, reject) => {
      connection.query(
        "INSERT INTO tahaptender SET ?",
        data,
        (error, result) => {
          if (!error) {
            const newResult = {
              id: result.insertId,
              ...data,
            };
            resolve(newResult);
          } else {
            reject(new Error("SQL : " + error.sqlMessage));
          }
        }
      );
    }),
  updateTahapTender: (data, id) =>
    new Promise((resolve, reject) => {
      connection.query(
        "UPDATE tahaptender SET ? WHERE id = ?",
        [data, id],
        (error, result) => {
          if (!error) {
            const newResult = {
              id,
              ...data,
            };
            resolve(newResult);
          } else {
            reject(new Error("SQL : " + error.sqlMessage));
          }
        }
      );
    }),
  deleteTahapTender: (id) =>
    new Promise((resolve, reject) => {
      connection.query(
        "DELETE FROM tahaptender WHERE id = ?",
        id,
        (error, result) => {
          if (!error) {
            resolve(id);
          } else {
            reject(new Error("SQL : " + error.sqlMessage));
          }
        }
      );
    }),
};
