const helper = require("../../helpers/wrapper");
const joinTenderModel = require("./joinTenderModel");
const userModel = require("../user/userModel");
const getAge = require("../../helpers/getAge");
const checkAlamat = require("../../helpers/checkAlamat");

const daysBetween = (date1, date2) => {
  // The number of milliseconds in one day
  const onDay = 1000 * 60 * 60 * 24;

  // Calculate the difference in milliseconds
  const differenceMs = Math.abs(date1 - date2);

  // Convert back to days and return
  return Math.round(differenceMs / onDay);
};

module.exports = {
  gedDataByIdTender: async (req, res) => {
    try {
      const { id, jabatan } = req.params;
      const result = await joinTenderModel.getJoinTenderByCondition(
        id,
        jabatan
      );
      if (result.length < 1) {
        return helper.response(res, 200, `Data ${id} tidak ditemukan !`, []);
      }
      for (const data of result) {
        const pengalaman = await userModel.getPengalamanByCondition({
          userId: data.userId,
        });
        let jumlahTahunKerja = 0;
        for (const dataPengalaman of pengalaman) {
          jumlahTahunKerja += daysBetween(
            dataPengalaman.tanggalMulai,
            dataPengalaman.tanggalSelesai
          );
        }
        data.jumlahTahunKerja = (jumlahTahunKerja / 365) | 0;
        data.jumlahPengalamanKerja = pengalaman.length;
        data.gaji = +data.gaji;
        data.usia = getAge(data.tanggalLahir);
        data.alamat = checkAlamat(data.provinsi, data.provinsiTender);
      }
      return helper.response(res, 200, "Success Get Data", result);
    } catch (error) {
      return helper.response(
        res,
        400,
        `Bad Request${error.message ? " (" + error.message + ")" : ""}`,
        null
      );
    }
  },
  gedDataByIdUser: async (req, res) => {
    try {
      const { id } = req.params;
      const result = await joinTenderModel.getJoinTenderByUser(id);
      return helper.response(res, 200, "Success Get Data", result);
    } catch (error) {
      return helper.response(
        res,
        400,
        `Bad Request${error.message ? " (" + error.message + ")" : ""}`,
        null
      );
    }
  },
  postDataJoinTender: async (req, res) => {
    try {
      const { userId, tenderId } = req.body;
      const check = await joinTenderModel.getJoinTenderByUserTender(
        tenderId,
        userId
      );
      if (check.length > 0) {
        return helper.response(
          res,
          400,
          "Anda sudah pernah mendaftar tender ini",
          null
        );
      }
      const result = await joinTenderModel.postJoinTender({ ...req.body });
      return helper.response(res, 200, "Sukses daftar tender", result);
    } catch (error) {
      return helper.response(
        res,
        400,
        `Bad Request${error.message ? " (" + error.message + ")" : ""}`,
        null
      );
    }
  },
  updateDataJoinTender: async (req, res) => {
    try {
      const { id } = req.params;
      const { statusSeleksi, statusDokumen } = req.body;
      const result = await joinTenderModel.getJoinByCondition({ id });
      if (result.length < 1) {
        return helper.response(res, 404, `Data ${id} tidak ditemukan !`, null);
      }
      let setData = {};
      if (statusSeleksi) {
        setData = { ...setData, statusSeleksi };
      }
      if (statusDokumen) {
        setData = { ...setData, statusDokumen };
      }

      await joinTenderModel.updateJoinTender(setData, id);
      return helper.response(res, 200, "Sukses update tender", {
        ...result[0],
        ...setData,
      });
    } catch (error) {
      return helper.response(
        res,
        400,
        `Bad Request${error.message ? " (" + error.message + ")" : ""}`,
        null
      );
    }
  },
  deleteDataJoinTender: async (req, res) => {
    try {
      const { id } = req.params;
      const result = await joinTenderModel.getJoinByCondition({ id });
      if (result.length < 1) {
        return helper.response(res, 404, `Data ${id} tidak ditemukan !`, null);
      }
      await joinTenderModel.deleteJoinTender(id);
      return helper.response(res, 200, "Sukses delete tender", result[0]);
    } catch (error) {
      return helper.response(
        res,
        400,
        `Bad Request${error.message ? " (" + error.message + ")" : ""}`,
        null
      );
    }
  },
};
