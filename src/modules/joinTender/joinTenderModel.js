const connection = require("../../config/mysql");

module.exports = {
  getJoinTenderByCondition: (id, jabatan) =>
    new Promise((resolve, reject) => {
      connection.query(
        "SELECT jt.id,t.provinsi AS provinsiTender, u.id AS userId, u.pendidikanTerakhir AS riwayatPendidikan, u.gaji AS gaji, u.provinsi AS provinsi, u.tanggalLahir AS tanggalLahir, u.nama, u.ska, jt.statusSeleksi, jt.statusDokumen, jt.jabatan FROM jointender AS jt JOIN user AS u ON jt.userId = u.id JOIN tender AS t ON jt.tenderId = t.id WHERE jt.tenderId = ? AND jt.jabatan = ? ",
        [id, jabatan],
        (error, result) => {
          !error
            ? resolve(result)
            : reject(new Error(`SQL : ${error.sqlMessage}`));
        }
      );
    }),
  getJoinByCondition: (data) =>
    new Promise((resolve, reject) => {
      connection.query(
        "SELECT * FROM jointender WHERE ? ",
        data,
        (error, result) => {
          !error
            ? resolve(result)
            : reject(new Error(`SQL : ${error.sqlMessage}`));
        }
      );
    }),
  getJoinTenderByUser: (data) =>
    new Promise((resolve, reject) => {
      connection.query(
        "SELECT t.id, t.kode, t.nama, jt.statusSeleksi, jt.statusDokumen, jt.jabatan FROM jointender AS jt JOIN tender AS t ON jt.tenderId = t.id WHERE jt.userId = ? ",
        data,
        (error, result) => {
          !error
            ? resolve(result)
            : reject(new Error(`SQL : ${error.sqlMessage}`));
        }
      );
    }),
  getJoinTenderByUserTender: (tenderId, userId) =>
    new Promise((resolve, reject) => {
      connection.query(
        "SELECT * FROM jointender WHERE tenderId = ? AND userId = ?",
        [tenderId, userId],
        (error, result) => {
          !error
            ? resolve(result)
            : reject(new Error(`SQL : ${error.sqlMessage}`));
        }
      );
    }),
  postJoinTender: (data) =>
    new Promise((resolve, reject) => {
      connection.query(
        "INSERT INTO jointender SET ?",
        data,
        (error, result) => {
          if (!error) {
            const newResult = {
              id: result.insertId,
              ...data,
            };
            resolve(newResult);
          } else {
            reject(new Error("SQL : " + error.sqlMessage));
          }
        }
      );
    }),
  updateJoinTender: (data, id) =>
    new Promise((resolve, reject) => {
      connection.query(
        "UPDATE jointender SET ? WHERE id = ?",
        [data, id],
        (error, result) => {
          if (!error) {
            const newResult = {
              id,
              ...data,
            };
            resolve(newResult);
          } else {
            reject(new Error("SQL : " + error.sqlMessage));
          }
        }
      );
    }),
  deleteJoinTender: (id) =>
    new Promise((resolve, reject) => {
      connection.query(
        "DELETE FROM jointender WHERE id = ?",
        id,
        (error, result) => {
          if (!error) {
            resolve(id);
          } else {
            reject(new Error("SQL : " + error.sqlMessage));
          }
        }
      );
    }),
};
