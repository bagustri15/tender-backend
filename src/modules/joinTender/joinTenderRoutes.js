const express = require("express");
const Route = express.Router();

const joinTenderController = require("./joinTenderController");

Route.get("/tender-id/:id/:jabatan", joinTenderController.gedDataByIdTender);
Route.get("/user-id/:id", joinTenderController.gedDataByIdUser);
Route.post("/", joinTenderController.postDataJoinTender);
Route.patch("/:id", joinTenderController.updateDataJoinTender);
Route.delete("/:id", joinTenderController.deleteDataJoinTender);

module.exports = Route;
