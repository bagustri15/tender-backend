const express = require("express");
const Route = express.Router();

const userController = require("./userController");
const uploadHelperUser = require("../../helpers/file/uploads/user");
const uploadHelperPajak = require("../../helpers/file/uploads/pajak");
const uploadHelperSka = require("../../helpers/file/uploads/ska");
const uploadHelperPengalaman = require("../../helpers/file/uploads/pengalaman");

Route.get("/profile/:id", userController.getDataById);
Route.patch("/password/:id", userController.updatePassword);
Route.patch("/profile/:id", uploadHelperUser, userController.updateProfile);
Route.patch("/pajak/:id", uploadHelperPajak, userController.updatePajak);

Route.get("/ska", userController.getDetailSka);
Route.post("/ska", uploadHelperSka, userController.postSka);
Route.patch("/ska/:id", uploadHelperSka, userController.updateSka);
Route.delete("/ska/:id", userController.deleteSka);

Route.post(
  "/pengalaman",
  uploadHelperPengalaman,
  userController.postPengalaman
);
Route.patch(
  "/pengalaman/:id",
  uploadHelperPengalaman,
  userController.updatePengalaman
);
Route.delete("/pengalaman/:id", userController.deletePengalaman);

module.exports = Route;
