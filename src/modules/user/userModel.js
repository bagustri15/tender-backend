const connection = require("../../config/mysql");

module.exports = {
  getDataConditions: (data) =>
    new Promise((resolve, reject) => {
      connection.query(
        "SELECT u.id, u.nama, u.tempatLahir, u.tanggalLahir, u.alamat, u.provinsi, u.kabupaten, u.kodePos, u.noTelp, u.pendidikanTerakhir, u.jurusan, u.fileIjazahTerakhir, u.gaji, u.email, u.role, u.ska AS setSka, u.nik, u.scanKtp, p.npwp, p.fileSptTahunan, p.fileSptBulanan, p.scanNpwp FROM user AS u JOIN pajak AS p ON u.id = p.userId WHERE u.id = ?",
        data,
        (error, result) => {
          !error
            ? resolve(result)
            : reject(new Error(`SQL : ${error.sqlMessage}`));
        }
      );
    }),
  updateDataUser: (data, id) =>
    new Promise((resolve, reject) => {
      connection.query(
        "UPDATE user SET ? WHERE id = ?",
        [data, id],
        (error, result) => {
          if (!error) {
            const newResult = {
              id,
              ...data,
            };
            resolve(newResult);
          } else {
            reject(new Error("SQL : " + error.sqlMessage));
          }
        }
      );
    }),
  updateDataPajak: (data, id) =>
    new Promise((resolve, reject) => {
      connection.query(
        "UPDATE pajak SET ? WHERE userId = ?",
        [data, id],
        (error, result) => {
          if (!error) {
            const newResult = {
              id,
              ...data,
            };
            resolve(newResult);
          } else {
            reject(new Error("SQL : " + error.sqlMessage));
          }
        }
      );
    }),
  getSkaByCondition: (data) =>
    new Promise((resolve, reject) => {
      connection.query("SELECT * FROM ska WHERE ? ", data, (error, result) => {
        !error
          ? resolve(result)
          : reject(new Error(`SQL : ${error.sqlMessage}`));
      });
    }),
  getDetailSka: (id, nama) =>
    new Promise((resolve, reject) => {
      connection.query(
        "SELECT * FROM ska WHERE userId = ? AND namaSertifikat = ? ",
        [id, nama],
        (error, result) => {
          !error
            ? resolve(result)
            : reject(new Error(`SQL : ${error.sqlMessage}`));
        }
      );
    }),
  postSka: (data) =>
    new Promise((resolve, reject) => {
      connection.query("INSERT INTO ska SET ?", data, (error, result) => {
        if (!error) {
          const newResult = {
            id: result.insertId,
            ...data,
          };
          resolve(newResult);
        } else {
          reject(new Error("SQL : " + error.sqlMessage));
        }
      });
    }),
  updateSka: (data, id) =>
    new Promise((resolve, reject) => {
      connection.query(
        "UPDATE ska SET ? WHERE id = ?",
        [data, id],
        (error, result) => {
          if (!error) {
            const newResult = {
              id,
              ...data,
            };
            resolve(newResult);
          } else {
            reject(new Error("SQL : " + error.sqlMessage));
          }
        }
      );
    }),
  deleteSka: (id) =>
    new Promise((resolve, reject) => {
      connection.query("DELETE FROM ska WHERE id = ?", id, (error, result) => {
        if (!error) {
          resolve(id);
        } else {
          reject(new Error("SQL : " + error.sqlMessage));
        }
      });
    }),
  getPengalamanByCondition: (data) =>
    new Promise((resolve, reject) => {
      connection.query(
        "SELECT id, nama, deskripsi, tanggalMulai, tanggalSelesai, fileBuktiKerja FROM pengalaman WHERE ? ",
        data,
        (error, result) => {
          !error
            ? resolve(result)
            : reject(new Error(`SQL : ${error.sqlMessage}`));
        }
      );
    }),
  postPengalaman: (data) =>
    new Promise((resolve, reject) => {
      connection.query(
        "INSERT INTO pengalaman SET ?",
        data,
        (error, result) => {
          if (!error) {
            const newResult = {
              id: result.insertId,
              ...data,
            };
            resolve(newResult);
          } else {
            reject(new Error("SQL : " + error.sqlMessage));
          }
        }
      );
    }),
  updatePengalaman: (data, id) =>
    new Promise((resolve, reject) => {
      connection.query(
        "UPDATE pengalaman SET ? WHERE id = ?",
        [data, id],
        (error, result) => {
          if (!error) {
            const newResult = {
              id,
              ...data,
            };
            resolve(newResult);
          } else {
            reject(new Error("SQL : " + error.sqlMessage));
          }
        }
      );
    }),
  deletePengalaman: (id) =>
    new Promise((resolve, reject) => {
      connection.query(
        "DELETE FROM pengalaman WHERE id = ?",
        id,
        (error, result) => {
          if (!error) {
            resolve(id);
          } else {
            reject(new Error("SQL : " + error.sqlMessage));
          }
        }
      );
    }),
};
