const helper = require("../../helpers/wrapper");
const userModel = require("./userModel");
const authModel = require("../auth/authModel");
const fileDelete = require("../../helpers/file/delete");
const bcrypt = require("bcrypt");

module.exports = {
  getDataById: async (req, res) => {
    try {
      const { id } = req.params;
      const result = await userModel.getDataConditions(id);
      if (result.length < 1) {
        return helper.response(res, 404, `Data ${id} tidak ditemukan !`, null);
      }
      const pengalaman = await userModel.getPengalamanByCondition({
        userId: id,
      });
      const ska = await userModel.getSkaByCondition({ userId: id });

      return helper.response(res, 200, "Sukses ambil data", {
        ...result[0],
        tanggalLahir: result[0].tanggalLahir
          ? result[0].tanggalLahir.toISOString().split("T")[0]
          : result[0].tanggalLahir,
        pengalaman,
        ska,
      });
    } catch (error) {
      return helper.response(
        res,
        400,
        `Bad Request${error.message ? " (" + error.message + ")" : ""}`,
        null
      );
    }
  },
  updatePassword: async (req, res) => {
    try {
      const { id } = req.params;
      const { oldPassword, newPassword, confirmPassword } = req.body;
      const result = await authModel.getDataConditions({ id });
      if (result.length < 1) {
        return helper.response(res, 404, `Data ${id} tidak ditemukan !`, null);
      }
      if (newPassword !== confirmPassword) {
        return helper.response(res, 400, `Password tidak sama !`, null);
      }
      const checkPassword = bcrypt.compareSync(oldPassword, result[0].password);
      if (!checkPassword) {
        return helper.response(res, 400, "Password lama salah !");
      }

      const salt = bcrypt.genSaltSync(10);
      const encryptPassword = bcrypt.hashSync(newPassword, salt);

      await userModel.updateDataUser(
        { password: encryptPassword, updatedAt: new Date(Date.now()) },
        id
      );

      delete result[0].password;
      return helper.response(res, 200, "Sukses update password", result);
    } catch (error) {
      return helper.response(
        res,
        400,
        `Bad Request${error.message ? " (" + error.message + ")" : ""}`,
        null
      );
    }
  },
  updateProfile: async (req, res) => {
    try {
      const { id } = req.params;
      const result = await userModel.getDataConditions(id);
      if (result.length < 1) {
        return helper.response(res, 404, `Data ${id} tidak ditemukan !`, null);
      }
      const setData = {
        ...req.body,
        scanKtp: req.files.scanKtp ? req.files.scanKtp[0].filename : "",
        fileIjazahTerakhir: req.files.fileIjazahTerakhir
          ? req.files.fileIjazahTerakhir[0].filename
          : "",
      };

      !setData.scanKtp
        ? delete setData.scanKtp
        : fileDelete.deleteFileUser(result[0].scanKtp);
      !setData.fileIjazahTerakhir
        ? delete setData.fileIjazahTerakhir
        : fileDelete.deleteFileUser(result[0].fileIjazahTerakhir);

      await userModel.updateDataUser(
        { ...setData, updatedAt: new Date(Date.now()) },
        id
      );

      return helper.response(res, 200, "Sukses update profile", {
        ...result[0],
        ...setData,
      });
    } catch (error) {
      return helper.response(
        res,
        400,
        `Bad Request${error.message ? " (" + error.message + ")" : ""}`,
        null
      );
    }
  },
  updatePajak: async (req, res) => {
    try {
      const { id } = req.params;
      const { npwp } = req.body;
      const result = await userModel.getDataConditions(id);
      if (result.length < 1) {
        return helper.response(res, 404, `Data ${id} tidak ditemukan !`, null);
      }
      const setData = {
        npwp,
        scanNpwp: req.files.scanNpwp ? req.files.scanNpwp[0].filename : "",
        fileSptTahunan: req.files.fileSptTahunan
          ? req.files.fileSptTahunan[0].filename
          : "",
        fileSptBulanan: req.files.fileSptBulanan
          ? req.files.fileSptBulanan[0].filename
          : "",
      };
      !setData.scanNpwp
        ? delete setData.scanNpwp
        : fileDelete.deleteFilePajak(result[0].scanNpwp);
      !setData.fileSptTahunan
        ? delete setData.fileSptTahunan
        : fileDelete.deleteFilePajak(result[0].fileSptTahunan);
      !setData.fileSptBulanan
        ? delete setData.fileSptBulanan
        : fileDelete.deleteFilePajak(result[0].fileSptBulanan);

      await userModel.updateDataPajak(
        { ...setData, updatedAt: new Date(Date.now()) },
        id
      );
      return helper.response(res, 200, "Sukses update pajak", {
        ...result[0],
        ...setData,
      });
    } catch (error) {
      return helper.response(
        res,
        400,
        `Bad Request${error.message ? " (" + error.message + ")" : ""}`,
        null
      );
    }
  },
  getDetailSka: async (req, res) => {
    try {
      const { userId, namaSertifikat } = req.query;
      const result = await userModel.getDetailSka(userId, namaSertifikat);
      if (result.length < 1) {
        return helper.response(res, 404, `Data tidak ditemukan !`, null);
      }
      return helper.response(res, 200, "Sukses ambil data", result);
    } catch (error) {
      return helper.response(
        res,
        400,
        `Bad Request${error.message ? " (" + error.message + ")" : ""}`,
        null
      );
    }
  },
  postSka: async (req, res) => {
    try {
      const setData = {
        ...req.body,
        fileSertifikat: req.file ? req.file.filename : "",
      };
      const result = await userModel.postSka(setData);
      return helper.response(
        res,
        200,
        "Sukses tambah sertifikat keahlian",
        result
      );
    } catch (error) {
      return helper.response(
        res,
        400,
        `Bad Request${error.message ? " (" + error.message + ")" : ""}`,
        null
      );
    }
  },
  updateSka: async (req, res) => {
    try {
      const { id } = req.params;
      const result = await userModel.getSkaByCondition({ id });
      if (result.length < 1) {
        return helper.response(res, 404, `Data ${id} tidak ditemukan !`, null);
      }
      const setData = {
        ...req.body,
        fileSertifikat: req.file ? req.file.filename : "",
      };
      !setData.fileSertifikat
        ? delete setData.fileSertifikat
        : fileDelete.deleteFileSka(result[0].fileSertifikat);

      await userModel.updateSka(
        { ...setData, updatedAt: new Date(Date.now()) },
        id
      );
      return helper.response(res, 200, "Sukses update sertifikat keahlian", {
        ...result[0],
        ...setData,
      });
    } catch (error) {
      return helper.response(
        res,
        400,
        `Bad Request${error.message ? " (" + error.message + ")" : ""}`,
        null
      );
    }
  },
  deleteSka: async (req, res) => {
    try {
      const { id } = req.params;
      const result = await userModel.getSkaByCondition({ id });
      if (result.length < 1) {
        return helper.response(res, 404, `Data ${id} tidak ditemukan !`, null);
      }
      fileDelete.deleteFileSka(result[0].fileSertifikat);
      await userModel.deleteSka(id);
      return helper.response(
        res,
        200,
        "Sukses delete sertifikat keahlian",
        result[0]
      );
    } catch (error) {
      return helper.response(
        res,
        400,
        `Bad Request${error.message ? " (" + error.message + ")" : ""}`,
        null
      );
    }
  },
  postPengalaman: async (req, res) => {
    try {
      const setData = {
        ...req.body,
        fileBuktiKerja: req.file ? req.file.filename : "",
      };
      const result = await userModel.postPengalaman(setData);
      return helper.response(res, 200, "Sukses tambah pengalaman", result);
    } catch (error) {
      return helper.response(
        res,
        400,
        `Bad Request${error.message ? " (" + error.message + ")" : ""}`,
        null
      );
    }
  },
  updatePengalaman: async (req, res) => {
    try {
      const { id } = req.params;
      const result = await userModel.getPengalamanByCondition({
        id,
      });
      const setData = {
        ...req.body,
        fileBuktiKerja: req.file ? req.file.filename : "",
      };
      !setData.fileBuktiKerja
        ? delete setData.fileBuktiKerja
        : fileDelete.deleteFilePengalaman(result[0].fileBuktiKerja);

      await userModel.updatePengalaman(
        { ...setData, updatedAt: new Date(Date.now()) },
        id
      );
      return helper.response(res, 200, "Sukses update pengalaman", result);
    } catch (error) {
      return helper.response(
        res,
        400,
        `Bad Request${error.message ? " (" + error.message + ")" : ""}`,
        null
      );
    }
  },
  deletePengalaman: async (req, res) => {
    try {
      const { id } = req.params;
      const result = await userModel.getPengalamanByCondition({
        id,
      });
      fileDelete.deleteFilePengalaman(result[0].fileBuktiKerja);
      await userModel.deletePengalaman(id);
      return helper.response(res, 200, "Sukses delete pengalaman", { result });
    } catch (error) {
      return helper.response(
        res,
        400,
        `Bad Request${error.message ? " (" + error.message + ")" : ""}`,
        null
      );
    }
  },
};
