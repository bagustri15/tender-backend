const helper = require("../../helpers/wrapper");
const dataTrainingModel = require("./dataTrainingModel");

module.exports = {
  getDataTraining: async (req, res) => {
    try {
      const result = await dataTrainingModel.getDataTraining();
      return helper.response(res, 200, "Success Get Data", result);
    } catch (error) {
      return helper.response(
        res,
        400,
        `Bad Request${error.message ? " (" + error.message + ")" : ""}`,
        null
      );
    }
  },
  getDataTrainingByCondition: async (req, res) => {
    try {
      if (req.body.length < 1) {
        return helper.response(res, 400, "Data tidak ditemukan", result);
      }
      let query = "";
      req.body.map((item) => {
        if (item.results !== null) {
          const { desc, field } = item.results;
          if (!query) {
            query += `${field} = '${desc}'`;
          } else {
            query += ` AND ${field} = '${desc}'`;
          }
        }
      });

      const result = await dataTrainingModel.getDataTrainingCondition(query);
      return helper.response(res, 200, "Success Get Data Condition", result);
    } catch (error) {
      return helper.response(
        res,
        400,
        `Bad Request${error.message ? " (" + error.message + ")" : ""}`,
        null
      );
    }
  },
  getResult: async (req, res) => {
    try {
      const result = await dataTrainingModel.getDataResult();
      return helper.response(res, 200, "Success Get Data", result[0]);
    } catch (error) {
      return helper.response(
        res,
        400,
        `Bad Request${error.message ? " (" + error.message + ")" : ""}`,
        null
      );
    }
  },
  updateResult: async (req, res) => {
    try {
      console.log(req.body);
      const setData = {
        result: JSON.stringify(req.body),
      };
      const result = await dataTrainingModel.updateResult(setData);
      return helper.response(res, 200, "Success Patch Data", result);
    } catch (error) {
      return helper.response(
        res,
        400,
        `Bad Request${error.message ? " (" + error.message + ")" : ""}`,
        null
      );
    }
  },
};
