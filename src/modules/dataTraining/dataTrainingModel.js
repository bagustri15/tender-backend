const connection = require("../../config/mysql");

module.exports = {
  getDataTraining: () =>
    new Promise((resolve, reject) => {
      connection.query("SELECT * FROM dataTraining", (error, result) => {
        !error
          ? resolve(result)
          : reject(new Error("SQL : " + error.sqlMessage));
      });
    }),
  getDataTrainingCondition: (condition) =>
    new Promise((resolve, reject) => {
      connection.query(
        `SELECT * FROM dataTraining WHERE ${condition}`,
        (error, result) => {
          !error
            ? resolve(result)
            : reject(new Error("SQL : " + error.sqlMessage));
        }
      );
    }),
  getDataResult: () =>
    new Promise((resolve, reject) => {
      connection.query("SELECT * FROM resultCondition", (error, result) => {
        !error
          ? resolve(result)
          : reject(new Error("SQL : " + error.sqlMessage));
      });
    }),
  updateResult: (data) =>
    new Promise((resolve, reject) => {
      connection.query(
        "UPDATE resultCondition SET ? WHERE id = 1",
        data,
        (error, result) => {
          if (!error) {
            const newResult = {
              id,
              ...data,
            };
            resolve(newResult);
          } else {
            reject(new Error("SQL : " + error.sqlMessage));
          }
        }
      );
    }),
};
