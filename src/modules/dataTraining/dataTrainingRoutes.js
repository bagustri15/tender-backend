const express = require("express");
const Route = express.Router();

const dataTrainingController = require("./dataTrainingController");

Route.get("/", dataTrainingController.getDataTraining);
Route.post("/condition", dataTrainingController.getDataTrainingByCondition);
Route.get("/result", dataTrainingController.getResult);
Route.patch("/", dataTrainingController.updateResult);

module.exports = Route;
